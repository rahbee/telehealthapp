package com.sinepulse.swipe;

/**
 * All files including this pakage are used to swipe list item right to left in Doctor Shedule fragment.
 *
 * source https://github.com/baoyongzhang/SwipeMenuListView 
 *
 * @author misba
 */

public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
