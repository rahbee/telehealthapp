package com.sinepulse.telehealthapp;

/**
 * This file is used to login module.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for login
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.telehealthapp.customize.Validation;

public class Login extends Activity implements OnClickListener {

	private EditText etUserName, etUserPassword;
	private Button btnLogin;
	private TextView tvTermCondition, tvCreateAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {

		etUserName = (EditText) findViewById(R.id.etUserName);
		Font.RobotoRegular(Login.this, etUserName);
		etUserPassword = (EditText) findViewById(R.id.etUserPassword);
		Font.RobotoRegular(Login.this, etUserPassword);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		Font.RobotoRegular(Login.this, btnLogin);
		btnLogin.setOnClickListener(this);

		tvCreateAccount = (TextView) findViewById(R.id.tvCreateAccount);
		Font.RobotoRegular(Login.this, tvCreateAccount);
		tvCreateAccount.setOnClickListener(this);

		tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);
		Font.RobotoThin(Login.this, tvTermCondition);
		tvTermCondition.setText(Html.fromHtml("<font color=\"#000000\">"
				+ "I agree to the " + "</font>" + "<font color=\"#27a2bc\">"
				+ "terms " + "</font>" + "<font color=\"#000000\">" + "and "
				+ "</font>" + "<font color=\"#27a2bc\">" + "conditions "
				+ "</font>"));
	}

	/**
	 * Go SignUp page
	 * 
	 * @param
	 * @return
	 */
	public void goSignUp() {
		Intent sign_up = new Intent(Login.this, SignUp.class);
		startActivity(sign_up);
	}

	/**
	 * Go next activity after user successfully login
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO modal class depend on server respons needed
	 */
	public void GoNext() {
		// Send boolean intent(true/false) indicating user type (doctor/patient)
		Intent next = new Intent(Login.this, MainActivity.class);
		if (etUserName.getText().toString().equalsIgnoreCase("d")) {
			next.putExtra("user_type", true);
		} else {
			next.putExtra("user_type", false);
		}

		startActivity(next);

		// if(etUserName.getText().toString().equalsIgnoreCase("d")){
		// Intent next = new Intent(Login.this, DoctorProfileView.class);
		// next.putExtra("user_type", true);
		// startActivity(next);
		// }else{
		// Intent next = new Intent(Login.this, PatientProfileView.class);
		// next.putExtra("user_type", false);
		// startActivity(next);
		// }

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// reset();
	}

	public void reset() {
		etUserName.setText("");
		etUserPassword.setText("");
	}

	/**
	 * Checking for valid input and verified user from server.
	 *
	 * @param
	 * @return
	 * 
	 *         TODO api implementation needed for verified user
	 */
	public void checkInput() {
		// login validation status [true = required]
		if (Validation.isValidate) {

			if (Validation.IsEmpty(etUserName)) {
				etUserName.setError("Username Required"); // Username Required
				return;
			} else if (Validation.IsEmpty(etUserPassword)) {
				etUserPassword.setError("Password Required");
				return;
			}
		}

		// Here server varification needed

		GoNext();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnLogin:
			checkInput();
			break;
		case R.id.tvCreateAccount:
			goSignUp();
			break;

		default:
			break;
		}
	}

}
