package com.sinepulse.telehealthapp;

/**
 * This file is used to SignUp for new user.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed 
 * 		 2. different validation action needed email,valid birth day etc
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.telehealthapp.customize.Validation;

public class SignUp extends Activity implements OnClickListener {

	EditText etFirstName, etLastName, etEmailAddress, etPassword, etMobileNo,
			etBirthDay;
	Button btnRegister;
	RadioButton radioMale, radioFemale, radioPatient, radioDoctor;
	TextView tvTermCondition;
	ImageView btnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up);

		initUI();
	}

	/**
	 * initial all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		etFirstName = (EditText) findViewById(R.id.etFirstName);
		Font.RobotoRegular(this, etFirstName);
		etLastName = (EditText) findViewById(R.id.etLastName);
		Font.RobotoRegular(this, etLastName);
		etEmailAddress = (EditText) findViewById(R.id.etEmailAddress);
		Font.RobotoRegular(this, etEmailAddress);
		etPassword = (EditText) findViewById(R.id.etPassword);
		Font.RobotoRegular(this, etPassword);
		etMobileNo = (EditText) findViewById(R.id.etMobileNo);
		Font.RobotoRegular(this, etMobileNo);
		etBirthDay = (EditText) findViewById(R.id.etBirthDay);
		Font.RobotoRegular(this, etBirthDay);
		etBirthDay.setOnClickListener(this);
		// etBirthDay.setEditableFactory(null);

		tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);

		Font.RobotoThin(SignUp.this, tvTermCondition);
		tvTermCondition.setText(Html.fromHtml("<font color=\"#000000\">"
				+ "I agree to the " + "</font>" + "<font color=\"#27a2bc\">"
				+ "terms " + "</font>" + "<font color=\"#000000\">" + "and "
				+ "</font>" + "<font color=\"#27a2bc\">" + "conditions "
				+ "</font>"));

		btnRegister = (Button) findViewById(R.id.btnRegister);
		Font.RobotoRegular(this, btnRegister);
		btnRegister.setOnClickListener(this);

		btnBack = (ImageView) findViewById(R.id.btnBack);
		// Font.RobotoRegular(this, btnBack);
		btnBack.setOnClickListener(this);

		radioDoctor = (RadioButton) findViewById(R.id.radioDoctor);
		Font.RobotoRegular(this, radioDoctor);
		radioPatient = (RadioButton) findViewById(R.id.radioPatient);
		Font.RobotoRegular(this, radioPatient);
		radioFemale = (RadioButton) findViewById(R.id.radioFemale);
		Font.RobotoRegular(this, radioFemale);
		radioMale = (RadioButton) findViewById(R.id.radioMale);
		Font.RobotoRegular(this, radioMale);
	}

	/**
	 * Checking for valid user input and complete registration process.
	 *
	 * @param
	 * @return
	 * 
	 *         TODO api implementation needed for registration process
	 */
	public void checkInput() {
		if (Validation.isValidate) {

			if (Validation.IsEmpty(etFirstName)) {
				etFirstName.setError("First name needed");
				return;
			} else if (Validation.IsEmpty(etLastName)) {
				etLastName.setError("Last name needed");
				return;
			} else if (Validation.IsEmpty(etEmailAddress)) {
				etEmailAddress.setError("Email address needed");
				return;
			} else if (!Validation.isValidEmail(etEmailAddress)) {
				etEmailAddress.setError("Invalid Email address");
				return;
			} else if (Validation.IsEmpty(etPassword)) {
				etPassword.setError("Password needed");
				return;
			} else if (Validation.IsEmpty(etMobileNo)) {
				etMobileNo.setError("Mobile no needed");
				return;
			}
		}

		// Here server varification needed

		GoNext();
	}

	/**
	 * back to the Login page
	 * 
	 * @param
	 * @return
	 */
	public void GoBack() {
		Intent back = new Intent(SignUp.this, Login.class);
		startActivity(back);
	}

	/**
	 * Go VerificationCode to complete user Registration
	 * 
	 * @param
	 * @return
	 */
	public void GoNext() {
		Intent next = new Intent(SignUp.this, VerificationCode.class);
		startActivity(next);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRegister:
			checkInput();
			break;
		case R.id.btnBack:
			// GoBack();
			break;
		case R.id.etBirthDay:
			showDatePicker();
			break;

		default:
			break;
		}
	}

	/**
	 * Date picker for select user birth day
	 * 
	 * Builds a custom dialog based on the defined layout
	 * 'res/layout/datepicker_layout.xml' and shows it
	 * 
	 * @param
	 * @return
	 */
	@SuppressLint("InflateParams")
	public void showDatePicker() {
		// Initializiation
		LayoutInflater inflater = getLayoutInflater();
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		View customView = inflater.inflate(R.layout.datepicker_layout, null);
		dialogBuilder.setView(customView);
		final Calendar now = Calendar.getInstance();
		final DatePicker datePicker = (DatePicker) customView
				.findViewById(R.id.dialog_datepicker);
		final TextView dateTextView = (TextView) customView
				.findViewById(R.id.dialog_dateview);
		final SimpleDateFormat dateViewFormatter = new SimpleDateFormat(
				"EEEE, dd.MM.yyyy", Locale.US);// GERMANY
		final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy",
				Locale.US);// GERMANY
		// Minimum date
		Calendar minDate = Calendar.getInstance();
		try {
			minDate.setTime(formatter.parse("12.12.1940"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		datePicker.setMinDate(minDate.getTimeInMillis());
		// View settings
		dialogBuilder.setTitle("Choose a date");
		Calendar choosenDate = Calendar.getInstance();
		int year = choosenDate.get(Calendar.YEAR);
		int month = choosenDate.get(Calendar.MONTH);
		int day = choosenDate.get(Calendar.DAY_OF_MONTH);
		try {
			Date choosenDateFromUI = formatter.parse(etBirthDay.getText()
					.toString());
			choosenDate.setTime(choosenDateFromUI);
			year = choosenDate.get(Calendar.YEAR);
			month = choosenDate.get(Calendar.MONTH);
			day = choosenDate.get(Calendar.DAY_OF_MONTH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Calendar dateToDisplay = Calendar.getInstance();
		dateToDisplay.set(year, month, day);
		dateTextView.setText(dateViewFormatter.format(dateToDisplay.getTime()));
		// Buttons
		dialogBuilder.setNegativeButton("Go to today",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						etBirthDay.setText(formatter.format(now.getTime()));
						dialog.dismiss();
					}
				});
		dialogBuilder.setPositiveButton("Choose",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Calendar choosen = Calendar.getInstance();
						choosen.set(datePicker.getYear(),
								datePicker.getMonth(),
								datePicker.getDayOfMonth());
						etBirthDay.setText(dateViewFormatter.format(choosen
								.getTime()));
						dialog.dismiss();
					}
				});
		final AlertDialog dialog = dialogBuilder.create();
		// Initialize datepicker in dialog atepicker
		datePicker.init(year, month, day,
				new DatePicker.OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar choosenDate = Calendar.getInstance();
						choosenDate.set(year, monthOfYear, dayOfMonth);
						dateTextView.setText(dateViewFormatter
								.format(choosenDate.getTime()));
						if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
								|| now.compareTo(choosenDate) < 0) {
							dateTextView.setTextColor(Color
									.parseColor("#ff0000"));
							dialog.getButton(DialogInterface.BUTTON_POSITIVE)
									.setEnabled(false);
						} else {
							dateTextView.setTextColor(Color
									.parseColor("#000000"));
							dialog.getButton(DialogInterface.BUTTON_POSITIVE)
									.setEnabled(true);
						}
					}
				});
		// Finish
		dialog.show();
	}

	/**
	 * Create JSONObject for Signup using user all information
	 * 
	 * @param
	 * @return JSONObject
	 * 
	 *         TODO JSONObject attachment with api implementation
	 */
	public JSONObject getParams() throws JSONException {
		JSONObject jOb = new JSONObject();

		JSONObject sign_up = new JSONObject();

		// add email address
		sign_up.put("email", etEmailAddress.getText().toString());

		// add password information
		JSONObject pass = new JSONObject();
		pass.put("first", etPassword.getText().toString());
		pass.put("second", etPassword.getText().toString());

		sign_up.put("plainPassword", pass);

		// add account type (doctor/patient)
		sign_up.put("type", "");

		// add profile information
		JSONObject profile = new JSONObject();

		profile.put("firstName", etFirstName.getText().toString());
		profile.put("lastName", etLastName.getText().toString());
		profile.put("gender", "");
		profile.put("mobile", etMobileNo.getText().toString());

		// add date of birth in profile information
		JSONObject dof = new JSONObject();

		dof.put("month", "");
		dof.put("day", "");
		dof.put("year", "");

		sign_up.put("profile", profile);

		//
		jOb.put("sinepulse_user_registration", sign_up);

		return jOb;

	}

}
