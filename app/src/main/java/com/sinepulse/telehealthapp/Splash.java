package com.sinepulse.telehealthapp;

/**
 * This file is used to show Splash Screen.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;

import com.sinepulse.telehealthapp.R;

public class Splash extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		/*
		 * New Handler to start the Menu-Activity and close this Splash-Screen
		 * after some seconds.
		 */
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				/* Create an Intent that will start the Menu-Activity. */
				Intent mainIntent = new Intent(Splash.this, Login.class);// SearchDoctor//DoctorBooking
				Splash.this.startActivity(mainIntent);
				// Splash.this.finish();
			}
		}, 2000);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		default:
			break;
		}
	}

}
