package com.sinepulse.telehealthapp.customize;

/**
 * This file is used to customize default ui text font 
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text in all ui(TextView,Button,EditText,RadioButton)
 * 
 * @author misba
 */

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class Font {

	/**
	 * Set Roboto-Regular font on TextView text
	 */
	public static void RobotoRegular(Context context, TextView txt) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		txt.setTypeface(face);
	}

	// Set Roboto-Thin font on TextView text
	public static void RobotoThin(Context context, TextView txt) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Thin.ttf");
		txt.setTypeface(face);
	}

	// Set Roboto-Medium font on TextView text
	public static void RobotoMedium(Context context, TextView txt) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Medium.ttf");
		txt.setTypeface(face);
	}

	/**
	 * Set Roboto-Regular on EditText text
	 */
	public static void RobotoRegular(Context context, EditText edit) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		edit.setTypeface(face);
	}

	// Set Roboto-Thin on EditText text
	public static void RobotoThin(Context context, EditText edit) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Thin.ttf");
		edit.setTypeface(face);
	}

	// Set Roboto-Medium on EditText text
	public static void RobotoMedium(Context context, EditText edit) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Medium.ttf");
		edit.setTypeface(face);
	}

	/**
	 * Set Roboto-Regular on Button text
	 */
	public static void RobotoRegular(Context context, Button btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		btn.setTypeface(face);
	}

	// Set Roboto-Thin on Button text
	public static void RobotoThin(Context context, Button btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Thin.ttf");
		btn.setTypeface(face);
	}

	// Set Roboto-Medium on Button text
	public static void RobotoMedium(Context context, Button btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Medium.ttf");
		btn.setTypeface(face);
	}

	/**
	 * Set Roboto-Regular on RadioButton text
	 */
	public static void RobotoRegular(Context context, RadioButton btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Regular.ttf");
		btn.setTypeface(face);
	}

	// Set Roboto-Thin on RadioButton text
	public static void RobotoThin(Context context, RadioButton btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Thin.ttf");
		btn.setTypeface(face);
	}

	// Set Roboto-Medium on RadioButton text
	public static void RobotoMedium(Context context, RadioButton btn) {
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"Roboto-Medium.ttf");
		btn.setTypeface(face);
	}

}
