package com.sinepulse.telehealthapp.customize;

import android.widget.EditText;

public class Validation {

	public final static boolean isValidate = false;

	/**
	 * 
	 */
	public static boolean IsEmpty(EditText edit) {
		if (edit != null) {
			return edit.getText().toString().trim().isEmpty();
		}
		return false;
	}

	/**
	 * 
	 */
	public final static boolean isValidEmail(String target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

	/**
	 * 
	 */
	public final static boolean isValidEmail(EditText edit) {

		if (!IsEmpty(edit)) {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(
					edit.getText().toString().trim()).matches();
		}

		return false;
	}

}
