package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to show doctor's available time slot on a particullar date.
 *
 * Patient can request for an appointment chosing a specific time also can see doctor's 
 * available time slot for last and next date
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for load doctor's available time slot
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.BookingAdapter;

public class DoctorBooking extends Activity implements OnClickListener {

	GridView rTimeSlot;
	ImageView imgLeft, imgRight;
	TextView tvDate;

	BookingAdapter adapter;

	Calendar c;
	SimpleDateFormat df;
	String formattedDate;

	// int plus = 0, minus = 0;
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.doctor_booking);
		mContext = this;
		adapter = new BookingAdapter(this);

		c = Calendar.getInstance();

		System.out.println("Current time => " + c.getTime());

		df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = df.format(c.getTime());

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		rTimeSlot = (GridView) findViewById(R.id.rTimeSlot);
		rTimeSlot.setAdapter(adapter);

		rTimeSlot.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				// Go appointment request form for this time interval
				GoRequest();
			}
		});

		imgLeft = (ImageView) findViewById(R.id.imgLeft);
		imgLeft.setOnClickListener(this);
		imgRight = (ImageView) findViewById(R.id.imgRight);
		imgRight.setOnClickListener(this);
		tvDate = (TextView) findViewById(R.id.tvDate);
		tvDate.setText(formattedDate);
	}

	/**
	 * set previous date on ui and reload doctor's free time slot from server
	 * for that date
	 * 
	 * @param
	 * @return
	 */
	public void prevDate() {
		c.add(Calendar.DATE, -1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	/**
	 * set next date on ui and reload doctor's free time slot from server for
	 * that date
	 * 
	 * @param
	 * @return
	 */
	public void nextDate() {
		c.add(Calendar.DATE, 1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	/**
	 * appointment request form
	 * 
	 * @param
	 * @return
	 */
	public void GoRequest() {
		Intent request = new Intent(mContext, AppointmentRequest.class);
		startActivity(request);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgLeft:
			// minus--;
			prevDate();
			break;
		case R.id.imgRight:
			// plus++;
			nextDate();
			break;

		default:
			break;
		}
	}
}
