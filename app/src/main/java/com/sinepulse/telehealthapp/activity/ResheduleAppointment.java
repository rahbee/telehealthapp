package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to reshedule requested appointment.
 * doctor or patient both can reshedule requested appointment.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for reshedule request with server communication
 * 		 2. set appropriate header view information for doctor and patient 
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.BookingAdapter;

public class ResheduleAppointment extends Activity implements OnClickListener {

	GridView rTimeSlot;
	ImageView imgLeft, imgRight, imgBack;
	TextView tvDate, txt_reshedule;
	Button btnSave, btnCancel;

	// header view
	RelativeLayout header_view;

	TextView tvTitleView;
	BookingAdapter adapter;

	Calendar c;
	SimpleDateFormat df;
	String formattedDate;

	int plus = 0, minus = 0;
	Context mContext;

	String Request_Type = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reshedule_appointment);

		mContext = this;
		adapter = new BookingAdapter(this);

		c = Calendar.getInstance();

		System.out.println("Current time => " + c.getTime());

		df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = df.format(c.getTime());

		try {
			Request_Type = getIntent().getStringExtra("RequestType");
			// doctor or patient
		} catch (Exception e) {
			e.printStackTrace();
		}

		initUI();
	}

	/**
	 * initial all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		header_view = (RelativeLayout) findViewById(R.id.header_view);

		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		tvTitleView.setText("Appointment Detail");

		rTimeSlot = (GridView) findViewById(R.id.rTimeSlot);
		rTimeSlot.setAdapter(adapter);

		rTimeSlot.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				GoRequest();
			}
		});

		imgLeft = (ImageView) findViewById(R.id.imgLeft);
		imgLeft.setOnClickListener(this);
		imgRight = (ImageView) findViewById(R.id.imgRight);
		imgRight.setOnClickListener(this);
		tvDate = (TextView) findViewById(R.id.tvDate);
		tvDate.setText(formattedDate);

		txt_reshedule = (TextView) findViewById(R.id.txt_reshedule);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setText("Reshedule");
		btnSave.setOnClickListener(this);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		// load header view (doctor or patient)
		header_view = (RelativeLayout) findViewById(R.id.header_view);
		if (Request_Type.equalsIgnoreCase("doctor")) {
			addDoctorHeader();
		} else {
			addPatientHeader();
		}
	}

	/**
	 * add header view on UI for doctor and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addDoctorHeader() {
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.appointment_header_doctor,
				header_view, false);
		header_view.addView(child);
	}

	/**
	 * add header view on UI for patient and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addPatientHeader() {
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(
				R.layout.appointment_header_patient, header_view, false);
		header_view.addView(child);
	}

	/**
	 * Get previous date for fecth doctor last day shedule from server
	 * 
	 * @param
	 * @return
	 */
	public void prevDate() {
		c.add(Calendar.DATE, -1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	/**
	 * Get next date for fecth doctor next day shedule from server
	 * 
	 * @param
	 * @return
	 */
	public void nextDate() {
		c.add(Calendar.DATE, 1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	public void GoRequest() {
		// Intent request = new Intent(mContext,
		// PatientAppointmentRequest.class);
		// startActivity(request);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgLeft:
			minus--;
			prevDate();
			break;
		case R.id.imgRight:
			plus++;
			nextDate();
			break;
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.btnBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
}
