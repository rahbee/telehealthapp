package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to reject requested appointment.
 * doctor or patient both can reject requested appointment.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for reject request with server communication
 * 		 2. set appropriate header view information for doctor and patient 
 */

import com.sinepulse.telehealthapp.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AppointmentRejection extends Activity implements OnClickListener {

	Button btnCancel, btnSave;
	EditText etDetail, etReject;
	TextView txt_title;
	ImageView imgBack;
	RelativeLayout header_view;

	String Request_Type = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointment_reject);

		try {
			Request_Type = getIntent().getStringExtra("RequestType");
			// doctor or patient
		} catch (Exception e) {

		}

		iniUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void iniUI() {
		txt_title = (TextView) findViewById(R.id.txt_title);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		etDetail = (EditText) findViewById(R.id.etDetail);
		etReject = (EditText) findViewById(R.id.etReject);

		// load header view (doctor or patient)
		header_view = (RelativeLayout) findViewById(R.id.header_view);
		if (Request_Type.equalsIgnoreCase("doctor")) {
			addDoctorHeader();
		} else {
			addPatientHeader();
		}
	}

	/**
	 * add header view on UI for doctor and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addDoctorHeader() {
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.appointment_header_doctor,
				header_view, false);
		header_view.addView(child);
	}

	/**
	 * add header view on UI for patient and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addPatientHeader() {
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(
				R.layout.appointment_header_patient, header_view, false);
		header_view.addView(child);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgBack:
			onBackPressed();
			break;
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
