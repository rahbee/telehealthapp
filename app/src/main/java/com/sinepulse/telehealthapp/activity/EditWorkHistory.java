package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to Add/Edit Doctor's work history Information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for add/edit work history information 
 * 		 2. implementation of save and cancel button action
 * 		
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class EditWorkHistory extends Activity implements OnClickListener {

	TextView tvTitleView, tvWorkTitle;
	EditText etOrgName, etJobTitle, etDetailResposibility;
	EditText tvFromDate, tvToDate;
	Button btnSave, btnCancel;
	Spinner spToDate, spFromDate;
	ImageView imgBack;

	Context mContext;

	String status = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_work_history);

		mContext = this;

		try {
			status = getIntent().getStringExtra("Status");
			// add/edit
		} catch (Exception e) {
			// TODO: handle exception
		}

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		Font.RobotoMedium(mContext, tvTitleView);

		tvWorkTitle = (TextView) findViewById(R.id.tvWorkTitle);
		Font.RobotoMedium(mContext, tvWorkTitle);

		etOrgName = (EditText) findViewById(R.id.etOrgName);
		Font.RobotoRegular(mContext, etOrgName);
		etJobTitle = (EditText) findViewById(R.id.etJobTitle);
		Font.RobotoRegular(mContext, etJobTitle);
		etDetailResposibility = (EditText) findViewById(R.id.etDetailResposibility);
		Font.RobotoRegular(mContext, etDetailResposibility);

		tvFromDate = (EditText) findViewById(R.id.tvFromDate);
		Font.RobotoRegular(mContext, tvFromDate);
		tvFromDate.setOnClickListener(this);
		tvToDate = (EditText) findViewById(R.id.tvToDate);
		Font.RobotoRegular(mContext, tvToDate);
		tvToDate.setOnClickListener(this);

		btnSave = (Button) findViewById(R.id.btnSave);
		Font.RobotoRegular(mContext, btnSave);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		Font.RobotoRegular(mContext, btnCancel);

		spToDate = (Spinner) findViewById(R.id.spToDate);
		spFromDate = (Spinner) findViewById(R.id.spFromDate);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		if (status.equalsIgnoreCase("add")) {
			tvTitleView.setText("Add Work History");
		} else {
			tvTitleView.setText("Edit Profile");
			setWorkHistory();
		}
	}

	/**
	 * set current Work History on ui
	 * 
	 * @param
	 * @return
	 */
	public void setWorkHistory() {
		etJobTitle.setText("");
		etOrgName.setText("");
		etDetailResposibility.setText("");
		tvFromDate.setText("");
		tvToDate.setText("");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.imgBack:
			onBackPressed();
			break;
		case R.id.tvFromDate:

			break;
		case R.id.tvToDate:

			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
}
