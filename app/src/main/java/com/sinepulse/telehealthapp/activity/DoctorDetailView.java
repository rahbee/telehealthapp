package com.sinepulse.telehealthapp.activity;

/**
 * Patient can see doctor's detail information (speciality,about him etc).
 *
 * @author misba
 * 
 * TODO 1. the doctor shedule data is different for different doctor's. so shedule data should be loaded dynamically 
 * and accesible from other class. Now static data is used to show one doctor shedule.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class DoctorDetailView extends Activity implements OnClickListener {

	ImageView imgBack;
	TextView tvTitleView, tvUserName, tvSpeciality, tvMyStory, tvUserId;
	RelativeLayout rBalance;
	TableLayout tbSpeciality;
	Context mContext;

	WebView tvAboutMe;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.doctor_detail);
		mContext = this;
		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @WebVeiw used for show about me(info) to use text justify
	 * @param
	 * @return
	 */
	public void initUI() {
		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		tvTitleView.setText("Doctor Profile");
		Font.RobotoMedium(mContext, tvTitleView);

		rBalance = (RelativeLayout) findViewById(R.id.rBalance);
		rBalance.setVisibility(View.INVISIBLE);

		tvUserName = (TextView) findViewById(R.id.tvUserName);
		Font.RobotoMedium(mContext, tvUserName);
		tvSpeciality = (TextView) findViewById(R.id.tvSpeciality);
		Font.RobotoMedium(mContext, tvSpeciality);
		tvMyStory = (TextView) findViewById(R.id.tvMyStory);
		Font.RobotoMedium(mContext, tvMyStory);
		// tvAboutMe = (TextView) findViewById(R.id.tvAboutMe);
		tvAboutMe = (WebView) findViewById(R.id.tvAboutMe);
		tvUserId = (TextView) findViewById(R.id.tvUserId);
		Font.RobotoThin(mContext, tvUserId);

		//
		addSpeciality();

		// set doctor profile information
		setProfileInfo();
	}

	/**
	 * set doctor's profile information
	 * 
	 * @param
	 * @return
	 */
	public void setProfileInfo() {
		String htmlText = "<html><body style=\"text-align:justify\"> %s </body></Html>";
		// webView1 = (WebView) findViewById(R.id.webView1);
		tvAboutMe.loadData(
				String.format(htmlText,
						getResources().getString(R.string.about_me)),
				"text/html", "utf-8");
	}

	/**
	 * show doctor's speciality
	 * 
	 * @param
	 * @return
	 */
	@SuppressLint("InflateParams")
	public void addSpeciality() {
		tbSpeciality = (TableLayout) findViewById(R.id.tbSpeciality);
		/* add doctor specialty */
		for (int i = 0; i < 3; i++) {

			View speciality = getLayoutInflater().inflate(
					R.layout.doctor_speciality, null);
			TextView tvSpeciality_01 = (TextView) speciality
					.findViewById(R.id.tvSpeciality_01);
			tvSpeciality_01.setText("Surgert handle part time");
			Font.RobotoRegular(mContext, tvSpeciality_01);

			View line = speciality.findViewById(R.id.line_space);

			if (i < 3) {
				line.setVisibility(View.VISIBLE);
			}

			tbSpeciality.addView(speciality);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
