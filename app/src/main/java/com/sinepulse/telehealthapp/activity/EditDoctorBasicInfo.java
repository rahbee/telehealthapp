package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to update Doctor's basic Information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for update Doctor's basic information 
 */

import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.telehealthapp.customize.Validation;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;

public class EditDoctorBasicInfo extends Activity implements OnClickListener {

	TextView tvTitleView, tvBasicInfo;
	ImageView imgBack;
	// basic info
	EditText etName, etEmail, etPhone, etBirthDay, etBloodType, etAddress,
			etPostCode;
	RadioButton radioMale, radioFemale;
	RadioGroup radioUserType;
	Button btnSave, btnCancel;
	Spinner spCity, spCountry;

	Context mContext;

	//
	ArrayAdapter<String> countryAdapter;
	ArrayAdapter<String> cityAdapter;

	String Country[] = { "United States", "Canada", "Afghanistan", "Albania",
			"Andorra", "Angola", "Anguilla", "Antarctica", "Barbuda",
			"Argentina", "Armenia", "Australia", "Azerbaijan", "Bangladesh" };
	String City[] = { "Comilla", "Rajshahi", "Chittagong", "Sylhet", "Dhaka",
			"Feni", "Anguilla", "Antarctica", "Barbuda", "Argentina",
			"Armenia", "Australia", "Azerbaijan", "Bangladesh" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_basic_info_doctor);

		mContext = this;

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		tvTitleView.setText("Edit Profile");
		Font.RobotoMedium(mContext, tvTitleView);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		// basic info
		tvBasicInfo = (TextView) findViewById(R.id.tvBasicInfo);
		Font.RobotoMedium(mContext, tvBasicInfo);

		etName = (EditText) findViewById(R.id.etName);
		Font.RobotoRegular(mContext, etName);
		etEmail = (EditText) findViewById(R.id.etEmail);
		Font.RobotoRegular(mContext, etEmail);
		etPhone = (EditText) findViewById(R.id.etPhone);
		Font.RobotoRegular(mContext, etPhone);
		etBirthDay = (EditText) findViewById(R.id.etBirthDay);
		Font.RobotoRegular(mContext, etBirthDay);
		etBloodType = (EditText) findViewById(R.id.etBloodType);
		Font.RobotoRegular(mContext, etBloodType);
		etAddress = (EditText) findViewById(R.id.etAddress);
		Font.RobotoRegular(mContext, etAddress);
		etPostCode = (EditText) findViewById(R.id.etPostCode);
		Font.RobotoRegular(mContext, etPostCode);

		spCity = (Spinner) findViewById(R.id.spCity);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		cityAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, City);
		cityAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCity.setAdapter(cityAdapter);

		spCountry = (Spinner) findViewById(R.id.spCountry);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		countryAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Country);
		countryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCountry.setAdapter(countryAdapter);

		radioUserType = (RadioGroup) findViewById(R.id.radioUserType);
		// Font.RobotoRegular(mContext, etPostCode);
		radioFemale = (RadioButton) findViewById(R.id.radioFemale);
		Font.RobotoRegular(mContext, radioFemale);
		radioMale = (RadioButton) findViewById(R.id.radioMale);
		Font.RobotoRegular(mContext, radioMale);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		Font.RobotoRegular(mContext, btnSave);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);
		Font.RobotoRegular(mContext, btnCancel);

		setProfileInfo();
	}

	/**
	 * different input validation
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO
	 */
	public void inputValidation() {

		if (etName.getText().toString().isEmpty()) {

		} else if (etEmail.getText().toString().isEmpty()) {

		} else if (!Validation.isValidEmail(etEmail)) {

		} else if (etPhone.getText().toString().isEmpty()) {

		} else if (etBirthDay.getText().toString().isEmpty()) {

		} else if (etName.getText().toString().isEmpty()) {

		} else if (etName.getText().toString().isEmpty()) {

		} else if (etName.getText().toString().isEmpty()) {

		} else if (etName.getText().toString().isEmpty()) {

		} else if (etName.getText().toString().isEmpty()) {

		}
	}

	/**
	 * set doctor basic information on ui
	 * 
	 * @param
	 * @return
	 */
	public void setProfileInfo() {

		etName.setText("Misba Alam");
		etEmail.setText("misba.sust@gmail.com");
		etPhone.setText("01193162508");
		etBirthDay.setText("4th June 1985");
		etBloodType.setText("A+(ve)");
		etAddress.setText("Faisal Mansion,Boshundhora R/A");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgBack:
			onBackPressed();
			break;
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
