package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to show possible type of appointment detail information.
 *
 * 1. Doctor start an appointment when appointment request accepted by doctor and payment complete by patient
 * 2. Doctor can reject an appointment request
 * 3. Doctor reshedule a requested appointment
 * 4. Patient can submit payment when appointment request is accepted by doctor
 * 
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for real time data. Now there is static data used to show different 
 * state of appointment status
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.thirdparty.CircularImageView;

public class AppointmentBasic extends Activity implements OnClickListener {

	CircularImageView imgProfile;
	ImageView imgBack;
	TextView tvTitleView, tvBalance;
	TextView tvAccept, tvReject, tvReshedule;
	TextView tvDetail, tvSubject, tvDateTime, tvUserName, tvPatientYear,
			tvBloodGroup, tvUserId;
	RelativeLayout rBalance, header_view;

	TextView tvMessage; // Message about Appointment Type (Approved,Reject etc)
	// OnClick doctor can start appointment with patient
	RelativeLayout lStartSession;
	RelativeLayout rPay;// OnClick patient can start the payment for appointment
	RelativeLayout rMiddleLayout;
	LinearLayout bottom_layout;
	//
	String Request_Type = "";// dortor or patient
	String AppointmentStatus = "";// approved,rejected,resheduled
	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointment_basic);
		mContext = this;

		try {
			Request_Type = getIntent().getStringExtra("RequestType");
			AppointmentStatus = getIntent().getStringExtra("AppointmentStatus");
		} catch (Exception e) {
			e.printStackTrace();
		}

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		header_view = (RelativeLayout) findViewById(R.id.header_view);
		rBalance = (RelativeLayout) findViewById(R.id.rBalance);
		if (Request_Type.equalsIgnoreCase("doctor")) {
			addDoctorHeader();
		} else {
			addPatientHeader();
		}

		imgProfile = (CircularImageView) findViewById(R.id.imgProfile);
		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);
		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		Font.RobotoMedium(mContext, tvTitleView);
		tvTitleView.setText("Appointment Detail");
		tvBalance = (TextView) findViewById(R.id.tvBalance);

		tvDetail = (TextView) findViewById(R.id.tvDetail);
		tvSubject = (TextView) findViewById(R.id.tvSubject);
		tvDateTime = (TextView) findViewById(R.id.tvDateTime);
		tvUserName = (TextView) findViewById(R.id.tvUserName);
		tvPatientYear = (TextView) findViewById(R.id.tvPatientYear);
		tvBloodGroup = (TextView) findViewById(R.id.tvBloodGroup);
		tvUserId = (TextView) findViewById(R.id.tvUserId);

		//
		tvMessage = (TextView) findViewById(R.id.tvMessage);
		rMiddleLayout = (RelativeLayout) findViewById(R.id.rMiddleLayout);
		lStartSession = (RelativeLayout) findViewById(R.id.lStartSession);
		lStartSession.setOnClickListener(this);
		rPay = (RelativeLayout) findViewById(R.id.rPay);
		rPay.setOnClickListener(this);
		bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);

		// option for Accept Reject and Reshedule
		tvAccept = (TextView) findViewById(R.id.tvAccept);
		tvAccept.setOnClickListener(this);
		tvReject = (TextView) findViewById(R.id.tvReject);
		tvReject.setOnClickListener(this);
		tvReshedule = (TextView) findViewById(R.id.tvReshedule);
		tvReshedule.setOnClickListener(this);

		//
		TakeActionOnAppointmentRequest();

	}

	/**
	 * different type of appointment state for doctor and patient are shown
	 * 
	 * @param
	 * @return
	 */
	public void TakeActionOnAppointmentRequest() {
		if (AppointmentStatus.equalsIgnoreCase("approved")) {
			// approved appointment request for doctor and patient
			if (Request_Type.equalsIgnoreCase("doctor")) {
				rMiddleLayout.setVisibility(View.VISIBLE);
				lStartSession.setVisibility(View.VISIBLE);
				bottom_layout.setVisibility(View.GONE);
				rPay.setVisibility(View.GONE);
				tvMessage.setVisibility(View.GONE);
			} else if (Request_Type.equalsIgnoreCase("patient")) {
				tvMessage.setVisibility(View.VISIBLE);
				tvMessage.setText(getResources().getText(
						R.string.request_status_approved));
				tvMessage.setBackgroundColor(getResources().getColor(
						R.color.request_status_approved));
				rMiddleLayout.setVisibility(View.VISIBLE);
				rPay.setVisibility(View.VISIBLE);
				lStartSession.setVisibility(View.GONE);
				bottom_layout.setVisibility(View.GONE);
			}

		} // rejected appointment request for doctor and patient
		else if (AppointmentStatus.equalsIgnoreCase("rejected")) {
			rMiddleLayout.setVisibility(View.GONE);

			tvMessage.setText(getResources().getText(
					R.string.request_status_reject));
			tvMessage.setBackgroundColor(getResources().getColor(
					R.color.request_status_reject));
			bottom_layout.setVisibility(View.INVISIBLE);
		} // resheduled appointment request for doctor and patient
		else if (AppointmentStatus.equalsIgnoreCase("resheduled")) {
			rMiddleLayout.setVisibility(View.GONE);

			if (Request_Type.equalsIgnoreCase("doctor")) {

				tvMessage
						.setText("your appointment request resheduled by patient");
				tvMessage.setBackgroundColor(getResources().getColor(
						R.color.request_status_reshedule));

				bottom_layout.setVisibility(View.VISIBLE);
				tvAccept.setVisibility(View.VISIBLE);
				tvReshedule.setVisibility(View.GONE);
				tvReject.setVisibility(View.VISIBLE);

			} else if (Request_Type.equalsIgnoreCase("patient")) {

				tvMessage.setText(getResources().getText(
						R.string.request_status_reshedule));
				tvMessage.setBackgroundColor(getResources().getColor(
						R.color.request_status_reshedule));

				bottom_layout.setVisibility(View.VISIBLE);
				tvAccept.setVisibility(View.VISIBLE);
				tvReshedule.setVisibility(View.GONE);
				tvReject.setVisibility(View.VISIBLE);
			}
		} // newly requested appointment for doctor and patient
		else if (AppointmentStatus.equalsIgnoreCase("")) {
			rMiddleLayout.setVisibility(View.GONE);
			tvMessage.setVisibility(View.GONE);

			if (Request_Type.equalsIgnoreCase("doctor")) {

				bottom_layout.setVisibility(View.VISIBLE);
				tvAccept.setVisibility(View.VISIBLE);
				tvReshedule.setVisibility(View.VISIBLE);
				tvReject.setVisibility(View.VISIBLE);
			} else {

				bottom_layout.setVisibility(View.INVISIBLE);
				rMiddleLayout.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * add header view on UI for doctor and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addDoctorHeader() {
		rBalance.setVisibility(View.INVISIBLE);
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.appointment_header_doctor,
				header_view, false);
		header_view.addView(child);
	}

	/**
	 * add header view on UI for patient and set corresponding profile
	 * information
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO set proper information in header view section
	 */
	public void addPatientHeader() {
		rBalance.setVisibility(View.VISIBLE);
		LayoutInflater layoutInflater = getLayoutInflater();
		View child = layoutInflater.inflate(
				R.layout.appointment_header_patient, header_view, false);
		header_view.addView(child);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.lStartSession:

			break;
		case R.id.rPay:

			break;

		case R.id.tvAccept:
			Toast.makeText(mContext, "Appointment accepted successfully",
					Toast.LENGTH_SHORT).show();
			finish();
			break;
		case R.id.tvReject:
			GoReject();
			break;
		case R.id.tvReshedule:
			GoResheduled();
			break;

		case R.id.imgBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	/**
	 * Go appointment rejection module
	 * 
	 * @param
	 * @return
	 */
	public void GoReject() {
		Intent reject = new Intent(AppointmentBasic.this,
				AppointmentRejection.class);
		reject.putExtra("RequestType", "doctor");
		startActivity(reject);
	}

	/**
	 * Go appointment reshedule module
	 * 
	 * @param
	 * @return
	 */
	public void GoResheduled() {
		Intent reshedule = new Intent(AppointmentBasic.this,
				ResheduleAppointment.class);
		reshedule.putExtra("RequestType", Request_Type);
		startActivity(reshedule);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
