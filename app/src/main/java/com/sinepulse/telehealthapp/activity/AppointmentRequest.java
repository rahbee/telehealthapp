package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to request an appointment on available time in a particullar date.
 *
 * Patient can request for an appointment chosing a specific time 
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * TODO 1. api implementation needed for successful appointment
 */

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class AppointmentRequest extends Activity implements OnClickListener {

	EditText etSubject, etDetail;
	Button btnRequest;
	TextView tvDateTime, tvRequestMsg;
	CheckBox chkEnable, chkTerm;
	// title bar
	ImageView imgBack;
	TextView tvTitleView, tvUserName, tvUserId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appointment_request);

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		etSubject = (EditText) findViewById(R.id.etSubject);
		Font.RobotoRegular(AppointmentRequest.this, etSubject);
		etDetail = (EditText) findViewById(R.id.etDetail);
		Font.RobotoRegular(AppointmentRequest.this, etDetail);

		btnRequest = (Button) findViewById(R.id.btnRequest);
		btnRequest.setOnClickListener(this);
		Font.RobotoRegular(AppointmentRequest.this, btnRequest);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		// Font.RobotoRegular(PatientAppointmentRequest.this, tvTitleView);
		tvDateTime = (TextView) findViewById(R.id.tvDateTime);
		Font.RobotoRegular(AppointmentRequest.this, tvDateTime);

		// textView display current date and time
		tvDateTime.setText(getCurrentDateTime());
		tvRequestMsg = (TextView) findViewById(R.id.tvRequestMsg);
		Font.RobotoRegular(AppointmentRequest.this, tvRequestMsg);

		tvUserName = (TextView) findViewById(R.id.tvUserName);
		// Font.RobotoRegular(PatientAppointmentRequest.this, tvUserName);
		tvUserId = (TextView) findViewById(R.id.tvUserId);
		Font.RobotoThin(AppointmentRequest.this, tvUserId);

		chkEnable = (CheckBox) findViewById(R.id.chkEnable);
		Font.RobotoThin(AppointmentRequest.this, chkEnable);
		chkTerm = (CheckBox) findViewById(R.id.chkTerm);
		Font.RobotoThin(AppointmentRequest.this, chkTerm);
		chkTerm.setText(Html.fromHtml("<font color=\"#000000\">"
				+ "I agree to the " + "</font>" + "<font color=\"#27a2bc\">"
				+ "terms " + "</font>" + "<font color=\"#000000\">" + "and "
				+ "</font>" + "<font color=\"#27a2bc\">" + "conditions "
				+ "</font>"));
	}

	/**
	 * get current date and time
	 * 
	 * @param
	 * @return
	 */
	public String getCurrentDateTime() {
		String currentDateTimeString = DateFormat.getDateTimeInstance().format(
				new Date());

		return currentDateTimeString;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRequest:
			Toast.makeText(getApplicationContext(),
					"Your requesting has been sent.", Toast.LENGTH_SHORT)
					.show();
			onBackPressed();
			break;
		case R.id.imgBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
