package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to edit patient profile information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation to update patient profile information 
 * 		 2. create output JSONObject including profile info and submit to the server for update profile information
 * 		 3. load city and country name from server or local storage. currently static data is used.
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;

public class EditProfile extends Activity implements OnClickListener {

	EditText etName, etEmail, etPhone, etBirthDay, etBloodType, etAddress,
			etCode;
	RadioButton radioMale, radioFemale;
	RadioGroup radioUserType;
	Button btnSave, btnCancel;
	ImageView imgBack;
	Spinner spCountry, spCity;

	TextView tvTitleView;

	ArrayAdapter<String> countryAdapter;
	ArrayAdapter<String> cityAdapter;

	String Country[] = { "Country", "United States", "Canada", "Afghanistan",
			"Albania", "Andorra", "Angola", "Anguilla", "Antarctica",
			"Barbuda", "Argentina", "Armenia", "Australia", "Azerbaijan",
			"Bangladesh" };
	String City[] = { "City", "Comilla", "Rajshahi", "Chittagong", "Sylhet",
			"Dhaka", "Feni" };

	String Profile_Type = "";

	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_basic_info_patient);

		Profile_Type = getIntent().getStringExtra("PROFILE_TYPE");
		// doctor or patient

		mContext = this;
		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		etName = (EditText) findViewById(R.id.etName);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etPhone = (EditText) findViewById(R.id.etPhone);
		etBirthDay = (EditText) findViewById(R.id.etBirthDay);
		etBirthDay.setOnClickListener(this);
		etBloodType = (EditText) findViewById(R.id.etBloodType);
		etAddress = (EditText) findViewById(R.id.etAddress);

		spCity = (Spinner) findViewById(R.id.spCity);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		cityAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, City);
		cityAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCity.setAdapter(cityAdapter);

		spCountry = (Spinner) findViewById(R.id.spCountry);
		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		countryAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Country);
		countryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spCountry.setAdapter(countryAdapter);

		radioUserType = (RadioGroup) findViewById(R.id.radioUserType);

		radioFemale = (RadioButton) findViewById(R.id.radioFemale);
		radioMale = (RadioButton) findViewById(R.id.radioMale);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		tvTitleView.setText("Edit Profile");

		setProfileInfo();

	}

	/**
	 * set all profile information in UI
	 * 
	 * @param
	 * @return
	 * 
	 */
	public void setProfileInfo() {

		etName.setText("Misba Alam");
		etEmail.setText("misba.sust@gmail.com");
		etPhone.setText("01193162508");
		etBirthDay.setText("4th June 1985");
		etBloodType.setText("A+(ve)");
		etAddress.setText("Faisal Mansion,Boshundhora R/A");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.imgBack:
			onBackPressed();
			break;
		case R.id.etBirthDay:
			showDatePicker();
			break;

		default:
			break;
		}
	}

	/**
	 * Date picker for select user birth day
	 * 
	 * Builds a custom dialog based on the defined layout
	 * 'res/layout/datepicker_layout.xml' and shows it
	 * 
	 * @param
	 * @return
	 */
	@SuppressLint("InflateParams")
	public void showDatePicker() {
		// Initializiation
		LayoutInflater inflater = getLayoutInflater();
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		View customView = inflater.inflate(R.layout.datepicker_layout, null);
		dialogBuilder.setView(customView);
		final Calendar now = Calendar.getInstance();
		final DatePicker datePicker = (DatePicker) customView
				.findViewById(R.id.dialog_datepicker);
		final TextView dateTextView = (TextView) customView
				.findViewById(R.id.dialog_dateview);
		final SimpleDateFormat dateViewFormatter = new SimpleDateFormat(
				"EEEE, dd.MM.yyyy", Locale.US);// GERMANY
		final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy",
				Locale.US);// GERMANY
		// Minimum date
		Calendar minDate = Calendar.getInstance();
		try {
			minDate.setTime(formatter.parse("12.12.1940"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		datePicker.setMinDate(minDate.getTimeInMillis());
		// View settings
		dialogBuilder.setTitle("Choose a date");
		Calendar choosenDate = Calendar.getInstance();
		int year = choosenDate.get(Calendar.YEAR);
		int month = choosenDate.get(Calendar.MONTH);
		int day = choosenDate.get(Calendar.DAY_OF_MONTH);
		try {
			Date choosenDateFromUI = formatter.parse(etBirthDay.getText()
					.toString());
			choosenDate.setTime(choosenDateFromUI);
			year = choosenDate.get(Calendar.YEAR);
			month = choosenDate.get(Calendar.MONTH);
			day = choosenDate.get(Calendar.DAY_OF_MONTH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Calendar dateToDisplay = Calendar.getInstance();
		dateToDisplay.set(year, month, day);
		dateTextView.setText(dateViewFormatter.format(dateToDisplay.getTime()));
		// Buttons
		dialogBuilder.setNegativeButton("Go to today",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						etBirthDay.setText(formatter.format(now.getTime()));
						dialog.dismiss();
					}
				});
		dialogBuilder.setPositiveButton("Choose",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Calendar choosen = Calendar.getInstance();
						choosen.set(datePicker.getYear(),
								datePicker.getMonth(),
								datePicker.getDayOfMonth());
						etBirthDay.setText(dateViewFormatter.format(choosen
								.getTime()));
						dialog.dismiss();
					}
				});
		final AlertDialog dialog = dialogBuilder.create();
		// Initialize datepicker in dialog atepicker
		datePicker.init(year, month, day,
				new DatePicker.OnDateChangedListener() {
					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar choosenDate = Calendar.getInstance();
						choosenDate.set(year, monthOfYear, dayOfMonth);
						dateTextView.setText(dateViewFormatter
								.format(choosenDate.getTime()));
						if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
								|| now.compareTo(choosenDate) < 0) {
							dateTextView.setTextColor(Color
									.parseColor("#ff0000"));
							dialog.getButton(DialogInterface.BUTTON_POSITIVE)
									.setEnabled(false);
						} else {
							dateTextView.setTextColor(Color
									.parseColor("#000000"));
							dialog.getButton(DialogInterface.BUTTON_POSITIVE)
									.setEnabled(true);
						}
					}
				});
		// Finish
		dialog.show();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
