package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to Add/Edit Doctor's education Information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for add/edit education information 
 * 		 2. implementation of save and cancel button action
 * 		
 */

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class EditEducation extends Activity implements OnClickListener {

	Button btnSave, btnCancel;
	ImageView imgBack;
	TextView tvTitleView, tvEducation;
	//
	EditText etInstitution, etDegree, etMajorSubject, et_detail_degree;
	Spinner spPassingYear;

	// data adapter
	ArrayAdapter<String> dataYear;
	ArrayList<String> listYear;

	String status = "";// add , edit

	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_education);

		mContext = this;

		try {
			status = getIntent().getStringExtra("Status");
		} catch (Exception e) {
			e.printStackTrace();
		}

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		Font.RobotoRegular(mContext, btnSave);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);
		Font.RobotoRegular(mContext, btnCancel);

		tvTitleView = (TextView) findViewById(R.id.tvTitleView);

		Font.RobotoMedium(mContext, tvTitleView);
		tvEducation = (TextView) findViewById(R.id.tvEducation);
		Font.RobotoMedium(mContext, tvEducation);

		//
		etInstitution = (EditText) findViewById(R.id.etInstitution);
		Font.RobotoRegular(mContext, etInstitution);
		etDegree = (EditText) findViewById(R.id.etDegree);
		Font.RobotoRegular(mContext, etDegree);
		etMajorSubject = (EditText) findViewById(R.id.etMajorSubject);
		Font.RobotoRegular(mContext, etMajorSubject);
		et_detail_degree = (EditText) findViewById(R.id.et_detail_degree);
		Font.RobotoRegular(mContext, et_detail_degree);

		spPassingYear = (Spinner) findViewById(R.id.spPassingYear);
		setYear();
		dataYear = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, listYear);
		dataYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spPassingYear.setAdapter(dataYear);

		//
		if (status.equalsIgnoreCase("add")) {
			tvTitleView.setText("Add Education");
		} else {
			tvTitleView.setText("Edit Profile");
			setEducationInfomation();
		}

	}

	/**
	 * set year list for arrary adpter
	 * 
	 * @param
	 * @return
	 * 
	 *         TODO api implementation needed
	 */
	public void setYear() {
		listYear = new ArrayList<String>();

		listYear.add("2002");
		listYear.add("2003");
		listYear.add("2004");
		listYear.add("2005");
		listYear.add("2006");
		listYear.add("2007");
		listYear.add("2008");
		listYear.add("2009");
		listYear.add("2010");

		// dataYear.notifyDataSetChanged();
	}

	/**
	 * set doctor initial Education information on ui
	 * 
	 * @param
	 * @return
	 */
	public void setEducationInfomation() {
		etInstitution.setText("Dhaka Medical College");
		etDegree.setText("M.B.B.S");
		etMajorSubject.setText("Medicine");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.imgBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
