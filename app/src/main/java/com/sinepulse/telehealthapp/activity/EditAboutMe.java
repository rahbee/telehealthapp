package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to Edit Doctor's about me Information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for edit about me information 
 * 		 2. implementation of save and cancel button action
 * 		
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinepulse.telehealthapp.R;

public class EditAboutMe extends Activity implements OnClickListener {

	EditText etAboutMe;
	TextView tvAboutMe, tvTitleView;
	Button btnSave, btnCancel;
	ImageView imgBack;

	String AboutMe = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_about_me);

		try {
			AboutMe = getIntent().getStringExtra("AboutMe");
		} catch (Exception e) {
			e.printStackTrace();
		}

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		etAboutMe = (EditText) findViewById(R.id.etAboutMe);
		etAboutMe.setText(R.string.about_me);
		tvAboutMe = (TextView) findViewById(R.id.tvAboutMe);
		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		tvTitleView.setText("Edit Profile");

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);

		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);
	}

	/**
	 * user input validation
	 * 
	 * @param
	 * @return
	 */
	public void inputValidation() {
		if (etAboutMe.getText().toString().length() < 1) {
			Toast.makeText(getApplicationContext(),
					"Please write something about you!", Toast.LENGTH_SHORT)
					.show();
			return;
		}

		finish();
	}

	public void GoNext() {

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSave:
			inputValidation();
			break;
		case R.id.btnCancel:
			finish();
			break;
		case R.id.imgBack:
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
}
