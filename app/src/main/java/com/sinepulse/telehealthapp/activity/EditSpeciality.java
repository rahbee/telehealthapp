package com.sinepulse.telehealthapp.activity;

/**
 * This file is used to Add/Edit Doctor's speciality Information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for add/edit speciality information 
 * 		 2. implementation of save and cancel button action
 * 		
 */

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class EditSpeciality extends Activity implements OnClickListener {

	TextView tvTitleView, tvSpeciality;
	Button btnSave, btnCancel;
	EditText etSpeciality, etDetail;
	ImageView imgBack;

	String status = "";// add , edit

	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_speciality);

		mContext = this;

		try {
			status = getIntent().getStringExtra("Status");
			// add/edit
		} catch (Exception e) {
			// TODO: handle exception
		}

		initUI();
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		tvTitleView = (TextView) findViewById(R.id.tvTitleView);
		Font.RobotoMedium(this, tvTitleView);
		tvSpeciality = (TextView) findViewById(R.id.tvSpeciality);
		Font.RobotoRegular(this, tvSpeciality);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(this);
		Font.RobotoRegular(this, btnSave);

		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);
		Font.RobotoRegular(this, btnCancel);

		etSpeciality = (EditText) findViewById(R.id.etSpeciality);
		Font.RobotoRegular(this, etSpeciality);
		etDetail = (EditText) findViewById(R.id.etDetail);
		Font.RobotoRegular(this, etDetail);

		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		if (status.equalsIgnoreCase("add")) {
			tvTitleView.setText("Add Speciality");
		} else {
			tvTitleView.setText("Edit Profile");
			setSpecialityInformation();
		}
	}

	/**
	 * set current Speciality on ui
	 * 
	 * @param
	 * @return
	 */
	public void setSpecialityInformation() {
		etSpeciality.setText("M.B.B.S");
		etDetail.setText("");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSave:
			onBackPressed();
			break;
		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.imgBack:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

}
