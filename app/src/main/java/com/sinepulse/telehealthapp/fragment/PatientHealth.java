package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient health detail information.
 *
 * Patient can see a short desription of his health information. 
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. implementation of other health profile module [weight,blood pressure,ecg etc]
 */

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class PatientHealth extends Fragment implements OnClickListener {

	TextView blood_type, weight, height, blood_pressure;
	TextView tv_header, tv_blood_type, tv_weight, tv_height, tv_blood_pressure;

	ImageView imgBloodPressure, imgWeight, imgECG, imgPrescription, imgReports,
			imgOther;
	TextView tvOther, tvReports, tvPrescription, tvECG, tvWeight,
			tvBloodPressure;
	// bottom bar
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification;

	Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater
				.inflate(R.layout.health_view, container, false);
		mContext = getActivity();

		initUI(rootView);
		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {

		blood_type = (TextView) rootView.findViewById(R.id.blood_type);
		Font.RobotoRegular(mContext, blood_type);
		weight = (TextView) rootView.findViewById(R.id.weight);
		Font.RobotoRegular(mContext, weight);
		height = (TextView) rootView.findViewById(R.id.height);
		Font.RobotoRegular(mContext, height);
		blood_pressure = (TextView) rootView.findViewById(R.id.blood_pressure);
		Font.RobotoRegular(mContext, blood_pressure);

		tv_header = (TextView) rootView.findViewById(R.id.tv_header);
		Font.RobotoMedium(mContext, tv_header);

		tv_blood_type = (TextView) rootView.findViewById(R.id.tv_blood_type);
		Font.RobotoRegular(mContext, tv_blood_type);
		tv_weight = (TextView) rootView.findViewById(R.id.tv_weight);
		Font.RobotoRegular(mContext, tv_weight);
		tv_height = (TextView) rootView.findViewById(R.id.tv_height);
		Font.RobotoRegular(mContext, tv_height);
		tv_blood_pressure = (TextView) rootView
				.findViewById(R.id.tv_blood_pressure);
		Font.RobotoRegular(mContext, tv_blood_pressure);

		imgBloodPressure = (ImageView) rootView
				.findViewById(R.id.imgBloodPressure);
		imgBloodPressure.setOnClickListener(this);
		imgWeight = (ImageView) rootView.findViewById(R.id.imgWeight);
		imgWeight.setOnClickListener(this);
		imgECG = (ImageView) rootView.findViewById(R.id.imgECG);
		imgECG.setOnClickListener(this);
		imgPrescription = (ImageView) rootView
				.findViewById(R.id.imgPrescription);
		imgPrescription.setOnClickListener(this);
		imgReports = (ImageView) rootView.findViewById(R.id.imgReports);
		imgReports.setOnClickListener(this);
		imgOther = (ImageView) rootView.findViewById(R.id.imgOther);
		imgOther.setOnClickListener(this);

		//
		tvBloodPressure = (TextView) rootView
				.findViewById(R.id.tvBloodPressure);
		Font.RobotoThin(mContext, tvBloodPressure);
		tvWeight = (TextView) rootView.findViewById(R.id.tvWeight);
		Font.RobotoThin(mContext, tvWeight);
		tvECG = (TextView) rootView.findViewById(R.id.tvECG);
		Font.RobotoThin(mContext, tvECG);
		tvPrescription = (TextView) rootView.findViewById(R.id.tvPrescription);
		Font.RobotoThin(mContext, tvPrescription);
		tvReports = (TextView) rootView.findViewById(R.id.tvReports);
		Font.RobotoThin(mContext, tvReports);
		tvOther = (TextView) rootView.findViewById(R.id.tvOther);
		Font.RobotoThin(mContext, tvOther);

		// bottom bar
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		// imgMyAccount.setImageDrawable(getResources().getDrawable(
		// R.drawable.myhealth_pres));
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("My Health");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgBloodPressure:

			break;
		case R.id.imgWeight:

			break;
		case R.id.imgECG:

			break;
		case R.id.imgPrescription:

			break;
		case R.id.imgReports:

			break;
		case R.id.imgOther:

			break;
		// bottom bar layout
		case R.id.imgFindDoctor:
			GoSearch();
			break;
		case R.id.imgAppointment:
			patientAppointments();
			break;
		case R.id.imgNotification:
			GoNotifications();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;

		default:
			break;
		}
	}

	/**
	 * Go patient notifications list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotifications() {
		PatientNotification notify = new PatientNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoSearch() {

		DoctorSearch doctor = new DoctorSearch();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void patientAppointments() {
		PatientAppointmentList notify = new PatientAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient my account
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		PatientAccount account = new PatientAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
