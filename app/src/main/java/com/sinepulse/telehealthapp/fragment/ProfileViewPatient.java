package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient profile information.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation to fetch data from server to show patient profile information 
 */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.EditProfile;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.thirdparty.CircularImageView;

public class ProfileViewPatient extends Fragment implements OnClickListener {

	CircularImageView imgView; // profile image
	TextView tvUserName, tvUserId, tvBirthDate, tvBloodType;// , tvEditProfile;
	// RelativeLayout rEditProfile;
	TextView tvName, tvEmail, tvPhone, tvBirthDay, tvGender, tvInfo;
	View ContentView;
	// profile bottom bar
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification,
			imgEdit;

	Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.profile_view_patient,
				container, false);
		mContext = getActivity();
		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {

		// Profile Header
		tvUserName = (TextView) rootView.findViewById(R.id.tvUserName); // v.f
		Font.RobotoMedium(mContext, tvUserName);
		tvUserId = (TextView) rootView.findViewById(R.id.tvUserId);
		Font.RobotoRegular(mContext, tvUserId);
		tvBirthDate = (TextView) rootView.findViewById(R.id.tvBirthDate);
		Font.RobotoRegular(mContext, tvBirthDate);
		tvBloodType = (TextView) rootView.findViewById(R.id.tvBloodType);
		Font.RobotoRegular(mContext, tvBloodType);

		imgEdit = (ImageView) rootView.findViewById(R.id.imgEdit);
		imgEdit.setOnClickListener(this);

		tvInfo = (TextView) rootView.findViewById(R.id.tvInfo);
		Font.RobotoMedium(mContext, tvInfo);

		ContentView = rootView.findViewById(R.id.ContentView);

		tvName = (TextView) rootView.findViewById(R.id.tvName);
		Font.RobotoRegular(mContext, tvName);
		tvEmail = (TextView) rootView.findViewById(R.id.tvEmail);
		Font.RobotoRegular(mContext, tvEmail);
		tvPhone = (TextView) rootView.findViewById(R.id.tvPhone);
		Font.RobotoRegular(mContext, tvPhone);
		tvBirthDay = (TextView) rootView.findViewById(R.id.tvBirthDay);
		Font.RobotoRegular(mContext, tvBirthDay);
		tvGender = (TextView) rootView.findViewById(R.id.tvGender);
		Font.RobotoRegular(mContext, tvGender);

		// bottom bar of patient profile view
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("My Profile");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.imgFindDoctor:
			GoSearch();
			break;
		case R.id.imgAppointment:
			GoRequest();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgNotification:
			GoNitify();
			break;
		case R.id.imgEdit:
			GoEditProfile();
			break;

		default:
			break;
		}
	}

	/**
	 * Go patient edit profile fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoEditProfile() {
		Intent edit = new Intent(mContext, EditProfile.class);
		edit.putExtra("PROFILE_TYPE", "patient");
		startActivity(edit);
	}

	/**
	 * Go patient health fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyHealth() {

		PatientHealth health = new PatientHealth();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, health)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient notification fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNitify() {

		PatientNotification notify = new PatientNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoSearch() {
		DoctorSearch doctor = new DoctorSearch();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoRequest() {

		PatientAppointmentList appointment = new PatientAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		PatientAccount account = new PatientAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
