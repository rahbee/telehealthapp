package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show detail description of doctor profile information.
 * doctor can see his current balance and all invoices of completed transaction.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for doctor profile information from server
 * 		 2. set doctor profile information on ui
 */

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.EditAboutMe;
import com.sinepulse.telehealthapp.activity.EditDoctorBasicInfo;
import com.sinepulse.telehealthapp.activity.EditEducation;
import com.sinepulse.telehealthapp.activity.EditSpeciality;
import com.sinepulse.telehealthapp.activity.EditWorkHistory;
//import com.sinepulse.telehealthapp.BaseActivity;
import com.sinepulse.telehealthapp.customize.Font;

public class ProfileViewDoctor extends Fragment implements OnClickListener {

	// Profile Header
	TextView tvBasic, tvHealth, tvOthers;
	TextView txt_title, tvUserName, tvUserId;
	ImageView imgEdit;
	ScrollView ContentView;
	// profile bottom bar
	ImageView imgShedule, imgAppointment, imgMyAccount, imgNotification;

	Context mContext;
	View rootView = null;

	String AboutMe = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.profile_view_doctor, container,
				false);
		mContext = getActivity();

		initUI();

		// load doctor's basic information on ui
		inflateView(1);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {

		// User Profile Tab
		tvBasic = (TextView) rootView.findViewById(R.id.tvFirstPart);
		Font.RobotoRegular(mContext, tvBasic);
		tvBasic.setOnClickListener(this);
		tvBasic.setText("BASIC");
		tvHealth = (TextView) rootView.findViewById(R.id.tvSecondPart);
		Font.RobotoRegular(mContext, tvHealth);
		tvHealth.setOnClickListener(this);
		tvHealth.setText("ABOUT ME");
		tvOthers = (TextView) rootView.findViewById(R.id.tvThirdPart);
		Font.RobotoRegular(mContext, tvOthers);
		tvOthers.setOnClickListener(this);
		tvOthers.setText("OTHERS");

		// title bar
		txt_title = (TextView) rootView.findViewById(R.id.txt_title);
		Font.RobotoMedium(mContext, txt_title);

		//
		tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);
		Font.RobotoRegular(mContext, tvUserName);
		tvUserId = (TextView) rootView.findViewById(R.id.tvUserId);
		Font.RobotoThin(mContext, tvUserId);
		// tvEditProfile = (TextView) rootView.findViewById(R.id.tvEditProfile);
		// Font.RobotoThin(mContext, tvEditProfile);

		ContentView = (ScrollView) rootView.findViewById(R.id.ContentView);

		// bottom bar of doctor profile view
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgShedule = (ImageView) rootView.findViewById(R.id.imgShedule);
		imgShedule.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);

		imgEdit = (ImageView) rootView.findViewById(R.id.imgEdit);
		imgEdit.setOnClickListener(this);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("My Profile");
	}

	/**
	 * fucus current selected tab of doctor profile view
	 * 
	 * @param
	 * @return
	 */
	public void fucusedProfileBar(TextView txtView) {

		Drawable myIcon = this.getResources().getDrawable(
				R.drawable.highlight_bar);
		txtView.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
				myIcon);
		txtView.setCompoundDrawablePadding(20);

		txtView.setTextColor(getResources().getColor(R.color.txt_focused_color));

	}

	/**
	 * unfucused tab of doctor profile view
	 * 
	 * @param
	 * @return
	 */
	public void unfucusedProfileBar(TextView txtView) {

		Drawable myIcon = this.getResources().getDrawable(
				R.drawable.unfocused_bar);
		txtView.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
				myIcon);
		txtView.setCompoundDrawablePadding(20);
		txtView.setTextColor(getResources().getColor(
				R.color.txt_unfocused_color));
	}

	/**
	 * fucus selected tab and unfocused others tab
	 * 
	 * @param position
	 * @return
	 */
	public void loadContentView(int pos) {
		switch (pos) {
		case 1:// basic info
			fucusedProfileBar(tvBasic);
			unfucusedProfileBar(tvHealth);
			unfucusedProfileBar(tvOthers);
			break;
		case 2:
			fucusedProfileBar(tvHealth);
			unfucusedProfileBar(tvBasic);
			unfucusedProfileBar(tvOthers);
			break;// patient health
		case 3:// others
			fucusedProfileBar(tvOthers);
			unfucusedProfileBar(tvBasic);
			unfucusedProfileBar(tvHealth);
			break;
		default:
			break;
		}
	}

	/**
	 * This is doctor's Tab selection module (1.Basic 2.About Me 3.Others)
	 * 
	 * load curresponding ui component's on ui based on user tab selection
	 * 
	 * @param
	 * @return
	 */
	public void inflateView(int pos) {

		loadContentView(pos);

		// Parent layout
		RelativeLayout rContentBody = (RelativeLayout) rootView
				.findViewById(R.id.rContentBody);

		if (rContentBody.getChildCount() > 0) {
			rContentBody.removeAllViews();
		}

		// Layout inflater
		// LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View child = null;

		// load profile tab section

		switch (pos) {
		case 1:// basic info
			ContentView.setBackgroundResource(R.color.login_bg_color);
			child = doctorBasicInfo(rContentBody);
			break;
		case 2:// about me
			ContentView.setBackgroundResource(R.color.app_bg_color);
			child = doctorAboutMe(rContentBody);

			break;
		case 3:// education
			ContentView.setBackgroundResource(R.color.login_bg_color);
			child = doctorOthersInfo(rContentBody);
			break;

		default:
			break;
		}

		if (child != null) {
			rContentBody.addView(child);
		}

	}

	/**
	 * update doctor's about me information
	 * 
	 * @param
	 * @return
	 */
	public void EditAboutMe() {

		Intent about_me = new Intent(mContext, EditAboutMe.class);
		about_me.putExtra("AboutMe", AboutMe);
		startActivity(about_me);
	}

	/**
	 * load doctor's About Me Tab section on UI
	 * 
	 * @param RelativeLayout
	 * @return
	 */
	public View doctorAboutMe(RelativeLayout rContentBody) {
		// child = layoutInflater.inflate(R.layout.doctor_about_me,
		// rContentBody, false);
		// ContentView.setBackgroundResource(R.color.app_bg_color);

		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.doctor_about_me,
				rContentBody, false);

		TextView tv_about_me = (TextView) child.findViewById(R.id.tv_about_me);
		Font.RobotoRegular(mContext, tv_about_me);

		ImageView imgEditAboutMe = (ImageView) child
				.findViewById(R.id.imgEditAboutMe);
		imgEditAboutMe.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditAboutMe();
			}
		});

		TextView tv_header = (TextView) child.findViewById(R.id.tv_header);
		Font.RobotoRegular(mContext, tv_header);
		String htmlText = "<html><body style=\"text-align:justify\"> %s </body></Html>";
		WebView webView1 = (WebView) child.findViewById(R.id.webView1);
		webView1.loadData(
				String.format(htmlText,
						getResources().getString(R.string.about_me)),
				"text/html", "utf-8");

		return child;
	}

	/**
	 * load doctor basic information section on profile view fragment
	 * 
	 * @param RelativeLayout
	 * @return
	 */
	public View doctorBasicInfo(RelativeLayout rContentBody) {
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.patient_basic_info,
				rContentBody, false);

		ImageView imgEdit = (ImageView) child.findViewById(R.id.imgEdit);
		imgEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditDoctorProfile();
			}
		});

		// set doctor basic information on ui
		SetBasicInfo(child);

		return child;
	}

	/**
	 * set doctor basic information on ui
	 * 
	 * @param View
	 * @return
	 */
	public void SetBasicInfo(View Child) {
		TextView tvInfo = (TextView) Child.findViewById(R.id.tvInfo);
		Font.RobotoMedium(mContext, tvInfo);

		TextView tvName = (TextView) Child.findViewById(R.id.tvName);
		tvName.setText("Misba Alam");
		TextView tvEmail = (TextView) Child.findViewById(R.id.tvEmail);
		tvEmail.setText("misba.sust@gmail.com");
		TextView tvPhone = (TextView) Child.findViewById(R.id.tvPhone);
		tvPhone.setText("01193162508");
		TextView tvBirthDay = (TextView) Child.findViewById(R.id.tvBirthDay);
		tvBirthDay.setText("4th June 1987");
		TextView tvBloodType = (TextView) Child.findViewById(R.id.tvBloodType);
		tvBloodType.setText("Misba Alam");
		TextView tvGender = (TextView) Child.findViewById(R.id.tvGender);
		tvGender.setText("Misba Alam");
		TextView tvAddress = (TextView) Child.findViewById(R.id.tvAddress);
		tvAddress.setText("House  13, Road 02");
		TextView tvCode = (TextView) Child.findViewById(R.id.tvCode);
		tvCode.setText("Dhaka 1230, Bangladesh");
	}

	/**
	 * load doctor other information section on profile view fragment
	 * 
	 * @param
	 * @return
	 */
	@SuppressLint("InflateParams")
	public View doctorOthersInfo(RelativeLayout rContentBody) {

		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View child = layoutInflater.inflate(R.layout.doctor_others,
				rContentBody, false);

		/**
		 * add doctor's speciality section on UI
		 * 
		 * also add doctor's all speciality list
		 */

		// specialty header
		TextView tvSpeciality = (TextView) child
				.findViewById(R.id.tvSpeciality);
		Font.RobotoMedium(mContext, tvSpeciality);
		TableLayout tbSpeciality = (TableLayout) child
				.findViewById(R.id.tbSpeciality);

		TextView tvAddSpeciality = (TextView) child
				.findViewById(R.id.tvAddSpeciality);
		tvAddSpeciality.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddSpeciality();
			}
		});

		// add doctor specialty list item
		// here i should replace with speciality array list
		for (int i = 0; i < 3; i++) {

			View speciality = getActivity().getLayoutInflater().inflate(
					R.layout.doctor_speciality, null);
			TextView tvSpeciality_01 = (TextView) speciality
					.findViewById(R.id.tvSpeciality_01);
			tvSpeciality_01.setText("Surgert handle part time");
			Font.RobotoRegular(mContext, tvSpeciality_01);

			View line = speciality.findViewById(R.id.line_space);

			if (i < 3) {
				line.setVisibility(View.VISIBLE);
			}

			speciality.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditSpeciality();
				}
			});

			tbSpeciality.addView(speciality);
		}

		/**
		 * add doctor's education section on UI
		 * 
		 * also add doctor's all education information list
		 */

		// education header
		TextView tvEducation = (TextView) child.findViewById(R.id.tvEducation);
		Font.RobotoMedium(mContext, tvEducation);
		TableLayout tbEducation = (TableLayout) child
				.findViewById(R.id.tbEducation);

		TextView tvAddEducation = (TextView) child
				.findViewById(R.id.tvAddEducation);
		tvAddEducation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddEducation();
			}
		});

		// add doctor education list item
		// here i should replace with education array list
		for (int i = 0; i < 3; i++) {

			View edu = getActivity().getLayoutInflater().inflate(
					R.layout.doctor_education, null);

			TextView tvCourseTitle = (TextView) edu
					.findViewById(R.id.tvCourseTitle);
			Font.RobotoMedium(mContext, tvCourseTitle);
			TextView tvFaculty = (TextView) edu.findViewById(R.id.tvFaculty);
			Font.RobotoThin(mContext, tvFaculty);
			TextView tvConstitute = (TextView) edu
					.findViewById(R.id.tvConstitute);
			Font.RobotoRegular(mContext, tvConstitute);

			View line_space = edu.findViewById(R.id.line_space);

			if (i < 3) {
				line_space.setVisibility(View.VISIBLE);
			}

			edu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditEducation();
				}
			});

			tbEducation.addView(edu);
		}

		/**
		 * add doctor's work history section on UI
		 * 
		 * also add doctor's all work history list
		 */

		// work history header Job History
		TextView tvHistory = (TextView) child.findViewById(R.id.tvHistory);
		Font.RobotoMedium(mContext, tvHistory);
		TableLayout tbHistory = (TableLayout) child
				.findViewById(R.id.tbHistory);

		TextView tvAddHistory = (TextView) child
				.findViewById(R.id.tvAddHistory);
		tvAddHistory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddWorkHistory();
			}
		});

		// add doctor Job History list item
		// here i should replace with Job History array list
		for (int i = 0; i < 3; i++) {

			View history = getActivity().getLayoutInflater().inflate(
					R.layout.doctor_job_history, null);

			RelativeLayout rHistory = (RelativeLayout) history
					.findViewById(R.id.rHistory);
			rHistory.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});

			TextView tvJobDuration = (TextView) history
					.findViewById(R.id.tvJobDuration);
			Font.RobotoRegular(mContext, tvJobDuration);
			// tvJobDuration.setText("Surgert handle part time");
			TextView tvDesignation = (TextView) history
					.findViewById(R.id.tvDesignation);
			Font.RobotoRegular(mContext, tvDesignation);
			TextView tvInstitue = (TextView) history
					.findViewById(R.id.tvInstitue);
			Font.RobotoRegular(mContext, tvInstitue);

			View line_space = history.findViewById(R.id.line_space);

			if (i < 3) {
				line_space.setVisibility(View.VISIBLE);
			}

			history.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					EditWorkHistory();
				}
			});

			tbHistory.addView(history);
		}

		return child;
	}

	/**
	 * update doctor's secialityinformation
	 * 
	 * @param
	 * @return
	 */
	public void EditSpeciality() {
		Intent speciality = new Intent(mContext, EditSpeciality.class);
		speciality.putExtra("Status", "edit");
		startActivity(speciality);
	}

	/**
	 * update doctor's education information
	 * 
	 * @param
	 * @return
	 */
	public void EditEducation() {
		Intent education = new Intent(mContext, EditEducation.class);
		education.putExtra("Status", "edit");
		startActivity(education);
	}

	/**
	 * update doctor's work history
	 * 
	 * @param
	 * @return
	 */
	public void EditWorkHistory() {
		Intent history = new Intent(mContext, EditWorkHistory.class);
		history.putExtra("Status", "edit");
		startActivity(history);
	}

	/**
	 * update doctor's basic profile information
	 * 
	 * @param
	 * @return
	 */
	public void EditDoctorProfile() {
		Intent profile = new Intent(mContext, EditDoctorBasicInfo.class);
		startActivity(profile);
	}

	/**
	 * add doctor's new speciality information
	 * 
	 * @param
	 * @return
	 */
	public void AddSpeciality() {
		Intent add = new Intent(mContext, EditSpeciality.class);
		add.putExtra("Status", "add");
		startActivity(add);
	}

	/**
	 * add doctor's new education information
	 * 
	 * @param
	 * @return
	 */
	public void AddEducation() {
		Intent add = new Intent(mContext, EditEducation.class);
		add.putExtra("Status", "add");
		startActivity(add);
	}

	/**
	 * add doctor's new work history information
	 * 
	 * @param
	 * @return
	 */
	public void AddWorkHistory() {
		Intent add = new Intent(mContext, EditWorkHistory.class);
		add.putExtra("Status", "add");
		startActivity(add);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.tvFirstPart: // doctor basic info
			inflateView(1);
			break;
		case R.id.tvSecondPart: // doctor about me
			inflateView(2);
			break;
		case R.id.tvThirdPart: // doctor others
			inflateView(3);
			break;

		// bottom bar
		case R.id.imgNotification:
			GoNotify();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgAppointment:
			GoAppointments();
			break;
		case R.id.imgShedule:
			GoShedule();
			break;
		case R.id.imgEdit:
			//
			break;
		default:
			break;
		}
	}

	/**
	 * Go doctor notification fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotify() {
		DoctorNotification doctor = new DoctorNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor shedule fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoShedule() {
		DoctorShedule shedule = new DoctorShedule();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, shedule)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointments() {
		DoctorAppointmentList appointment = new DoctorAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		DoctorAccount account = new DoctorAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
