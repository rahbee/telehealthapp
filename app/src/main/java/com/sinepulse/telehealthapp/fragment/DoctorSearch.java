package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show all doctor's list.
 *
 * Patient can see a short desription of each doctor with their photo,current position and degree 
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for load doctor's detail information
 */

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.DoctorSearchAdapter;

public class DoctorSearch extends Fragment implements OnClickListener {

	Context mContext;

	ListView lvDoctorList;
	DoctorSearchAdapter adapter;
	TextView tvFee;

	// bottom bar tab
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.doctor_search, container,
				false);
		mContext = getActivity();
		adapter = new DoctorSearchAdapter(mContext);
		initUI(rootView);
		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {
		lvDoctorList = (ListView) rootView.findViewById(R.id.lvDoctorList);
		lvDoctorList.setAdapter(adapter);

		// bottom bar
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgFindDoctor.setImageDrawable(getResources().getDrawable(
				R.drawable.find_doctor_pres));
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);

		// tvFee = (TextView) rootView.findViewById(R.id.tvFee);
		// Font.RobotoThin(mContext, tvFee);
		// tvFee.setText(Html.fromHtml("<font color=\"#666666\">"
		// + "Fee : " + "</font>" + "<font color=\"#27a2bc\">"
		// + "800 Tk " + "</font>" ));
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("Search Doctor");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		// bottom bar
		case R.id.imgAppointment:
			GoAppointment();
			break;
		case R.id.imgFindDoctor:
			//
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgNotification:
			GoNotify();
			break;

		default:
			break;
		}
	}

	/**
	 * Go patient notification list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotify() {

		PatientNotification notify = new PatientNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointment() {

		PatientAppointmentList appointment = new PatientAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient health fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoHealth() {
		PatientHealth health = new PatientHealth();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, health)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		PatientAccount account = new PatientAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
