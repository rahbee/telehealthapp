package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient account related information.
 * Patient can see his current balance and all invoices of completed transaction.
 * <p/>
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 *
 * @author misba
 * @TODO 1. api implementation needed for patient invoice related information
 * 2. set patient profile information and invoice informations on ui
 */

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.PatientAccountAdapter;
import com.sinepulse.thirdparty.CircularImageView;

public class ChangePassword extends Fragment implements OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.change_password, container,
                false);

        return rootView;
    }

    @Override
    public void onClick(View v) {

    }

}
