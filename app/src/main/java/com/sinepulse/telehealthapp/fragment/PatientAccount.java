package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient account related information.
 * Patient can see his current balance and all invoices of completed transaction.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for patient invoice related information
 * 		 2. set patient profile information and invoice informations on ui
 */

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.PatientAccountAdapter;
import com.sinepulse.thirdparty.CircularImageView;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class PatientAccount extends Fragment implements OnClickListener {

	Context mContext;
	PatientAccountAdapter adapter;
	ListView lBalance;
	TextView tvBalance, tvUserName, tvUserId;
	CircularImageView imgProfile;

	// bottom bar
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.patient_account, container,
				false);
		mContext = getActivity();
		adapter = new PatientAccountAdapter(mContext);
		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {
		imgProfile = (CircularImageView) rootView.findViewById(R.id.imgProfile);
		
		
		tvBalance = (TextView) rootView.findViewById(R.id.tvBalance);
		tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);
		tvUserId = (TextView) rootView.findViewById(R.id.tvUserId);

		lBalance = (ListView) rootView.findViewById(R.id.lBalance);
		lBalance.setAdapter(adapter);
		lBalance.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

			}
		});

		// bottom bar
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		imgMyAccount.setImageDrawable(getResources().getDrawable(
				R.drawable.my_account_pres));
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		// bottom bar
		case R.id.imgFindDoctor:
			GoSearchDoctor();
			break;
		case R.id.imgAppointment:
			GoAppointments();
			break;
		case R.id.imgMyAccount:

			break;
		case R.id.imgNotification:
			GoNotification();
			break;

		default:
			break;
		}
	}

	/**
	 * Go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoSearchDoctor() {
		DoctorSearch search = new DoctorSearch();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, search)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointments() {
		PatientAppointmentList appointment = new PatientAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient notification fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotification() {
		PatientNotification notify = new PatientNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
