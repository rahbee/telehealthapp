package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show doctor's all notification.
 * doctor can see different types of notifications (pending,completed etc..).
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for show notification
 */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.AppointmentBasic;
import com.sinepulse.telehealthapp.adapter.PatientNotificationAdapter;

public class DoctorNotification extends Fragment implements OnClickListener {

	ListView lvNotify;
	// SwipeListView lvNotify;

	// bottom bar
	ImageView imgShedule, imgAppointment, imgMyAccount, imgNotification;

	PatientNotificationAdapter adapter;

	Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.doctor_notification,
				container, false);
		mContext = getActivity();
		adapter = new PatientNotificationAdapter(mContext);
		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {

		lvNotify = (ListView) rootView.findViewById(R.id.lvNotify);//
		// lvNotify = (SwipeListView) rootView.findViewById(R.id.lvNotify);

		lvNotify.setAdapter(adapter);
		// setSwipList();

		lvNotify.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				AppointmentDetail();
			}
		});

		// bottom bar
		imgShedule = (ImageView) rootView.findViewById(R.id.imgShedule);
		imgShedule.setOnClickListener(this);
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);

		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);
		imgNotification.setImageDrawable(getResources().getDrawable(
				R.drawable.notifications_pres));
	}

	/**
	 * Go doctor appointment detail fragment based on appointment status
	 * 
	 * @param
	 * @return
	 */
	public void AppointmentDetail() {
		Intent detail = new Intent(mContext, AppointmentBasic.class);
		detail.putExtra("RequestType", "doctor");
		detail.putExtra("AppointmentStatus", "");
		startActivity(detail);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("Notifications");
	}

	public int convertDpToPixel(float dp) {
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return (int) px;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		// bottom bar
		case R.id.imgShedule:
			GoShedule();
			break;
		case R.id.imgAppointment:
			GoAppointments();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgNotification:
			//
			break;

		default:
			break;
		}
	}

	/**
	 * Go doctor shedule fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoShedule() {
		DoctorShedule shedule = new DoctorShedule();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, shedule)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go appointment list frgament
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointments() {
		DoctorAppointmentList appointment = new DoctorAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		DoctorAccount account = new DoctorAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
