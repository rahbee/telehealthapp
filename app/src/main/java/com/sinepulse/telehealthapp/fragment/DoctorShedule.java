package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show doctor shedule.
 *
 * There are 3 types of shedule (3 different list item )
 * 1. available time slot that patient can request for appointment on this time and if doctor want's 
 * he can make this busy by right to left swipe
 * 2. busy time state . this is unavailable for patient to make appointmnet request
 * 3. already an appointment is accepted by a patient
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for load doctor's shedule data from server
 * 		 2. reload new shedule data when date is change( previous date or next date)
 * 		 3. set all data on ui
 * 		
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.swipe.SwipeMenu;
import com.sinepulse.swipe.SwipeMenuCreator;
import com.sinepulse.swipe.SwipeMenuItem;
import com.sinepulse.swipe.SwipeMenuListView;
import com.sinepulse.swipe.SwipeMenuListView.OnMenuItemClickListener;
import com.sinepulse.swipe.SwipeMenuListView.OnSwipeListener;
import com.sinepulse.telehealthapp.adapter.SheduleAdapter;

public class DoctorShedule extends Fragment implements OnClickListener {

	Context mContext;
	ImageView imgLeft, imgRight;
	TextView tvDate;
	// ListView lvShedule;
	SwipeMenuListView lvShedule;
	// SwipeListView lvShedule;

	// bottom bar
	ImageView imgShedule, imgAppointment, imgMyAccount, imgNotification;
	SheduleAdapter adapter;

	// AppAdapter adapter;

	Calendar c;
	SimpleDateFormat df;
	String formattedDate;

	int plus = 0, minus = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// setHasOptionsMenu(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.doctor_shedule, container,
				false);
		mContext = getActivity();

		adapter = new SheduleAdapter(mContext);
		// adapter = new AppAdapter();

		c = Calendar.getInstance();

		// System.out.println("Current time => " + c.getTime());

		df = new SimpleDateFormat("dd-MMM-yyyy");
		formattedDate = df.format(c.getTime());

		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {
		imgLeft = (ImageView) rootView.findViewById(R.id.imgLeft);
		imgLeft.setOnClickListener(this);
		imgRight = (ImageView) rootView.findViewById(R.id.imgRight);
		imgRight.setOnClickListener(this);

		tvDate = (TextView) rootView.findViewById(R.id.tvDate);
		tvDate.setText(formattedDate);

		// lvShedule = (ListView) rootView.findViewById(R.id.lvShedule);
		// lvShedule = (SwipeListView) rootView.findViewById(R.id.lvShedule);
		// lvShedule.setAdapter(adapter);
		// bottom bar
		imgShedule = (ImageView) rootView.findViewById(R.id.imgShedule);
		imgShedule.setOnClickListener(this);
		imgShedule.setImageDrawable(getResources().getDrawable(
				R.drawable.schedule_pres));
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);
		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);

		// use SwipeMenu library for swipping shedule list

		lvShedule = (SwipeMenuListView) rootView.findViewById(R.id.lvShedule);
		lvShedule.setAdapter(adapter);

		// step 1. create a MenuCreator
		SwipeMenuCreator creator = new SwipeMenuCreator() {

			@Override
			public void create(SwipeMenu menu) {
				// create "open" item

				// create "delete" item
				SwipeMenuItem deleteItem = new SwipeMenuItem(
						mContext.getApplicationContext());
				// set item background
				// deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
				// 0x3F, 0x25)));
				deleteItem.setBackground(new ColorDrawable(Color.RED));
				// set item width
				deleteItem.setWidth(dp2px(90));
				// set a icon
				deleteItem.setIcon(R.drawable.busy_white);

				// set item title
				deleteItem.setTitle("Busy");
				// set item title fontsize
				deleteItem.setTitleSize(18);
				// set item title font color
				deleteItem.setTitleColor(Color.WHITE);
				// add to menu

				// add to menu
				menu.addMenuItem(deleteItem);
			}
		};
		// set creator
		lvShedule.setMenuCreator(creator);

		// step 2. listener item click event
		lvShedule.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(int position, SwipeMenu menu,
					int index) {
				switch (index) {
				case 1:
					// delete
					// delete(item);
					// mAppList.remove(position);
					// mAdapter.notifyDataSetChanged();
					break;
				}
				return false;
			}
		});

		// set SwipeListener
		lvShedule.setOnSwipeListener(new OnSwipeListener() {

			@Override
			public void onSwipeStart(int position) {
				// swipe start
			}

			@Override
			public void onSwipeEnd(int position) {
				// swipe end
			}
		});

		// other setting
		// listView.setCloseInterpolator(new BounceInterpolator());

		// test item long click
		lvShedule.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Toast.makeText(mContext.getApplicationContext(),
				// position + " long click", 0).show();
				return false;
			}
		});
	}

	private int dp2px(int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				getResources().getDisplayMetrics());
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("My Schedule");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.doctor_schedule, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle item selection

		Toast.makeText(getActivity(), "toast", Toast.LENGTH_SHORT).show();
		switch (item.getItemId()) {
		case 1:
			return true;
		default:
			// return super.onOptionsItemSelected(item);
		}

		return true;
	}

	/**
	 * reload doctor's shedule data for last date from server and show them on
	 * ui
	 * 
	 * @param
	 * @return
	 */
	public void prevDate() {
		c.add(Calendar.DATE, -1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	/**
	 * reload doctor's shedule data for next date from server and show them on
	 * ui
	 * 
	 * @param
	 * @return
	 */
	public void nextDate() {
		c.add(Calendar.DATE, 1);
		formattedDate = df.format(c.getTime());

		tvDate.setText(formattedDate);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgLeft:
			minus--;
			prevDate();
			break;
		case R.id.imgRight:
			plus++;
			nextDate();
			break;
		// bottom bar
		case R.id.imgNotification:
			GoNotify();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgAppointment:
			GoAppointments();
			break;
		case R.id.imgShedule:
			//
			break;

		default:
			break;
		}
	}

	/**
	 * Go doctor's notication fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotify() {

		DoctorNotification doctor = new DoctorNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor's appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointments() {
		DoctorAppointmentList doctor = new DoctorAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor's account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		DoctorAccount account = new DoctorAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
