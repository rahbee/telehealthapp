package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show doctor account related information.
 * doctor can see his current balance and all invoices of completed transaction.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for doctor invoice related information
 * 		 2. set doctor profile information and invoice informations on ui
 */

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.DoctorAccountAdapter;
import com.sinepulse.thirdparty.CircularImageView;

public class DoctorAccount extends Fragment implements OnClickListener {

	Context mContext;

	// listview adapter
	DoctorAccountAdapter adapter;

	ListView lBalance;
	TextView tvBalance, tvUserName, tvUserId;
	CircularImageView imgProfile;

	// bottom bar tab
	ImageView imgShedule, imgAppointment, imgMyAccount, imgNotification;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.doctor_account, container,
				false);
		mContext = getActivity();
		adapter = new DoctorAccountAdapter(mContext);
		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {
		tvBalance = (TextView) rootView.findViewById(R.id.tvBalance);
		tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);
		tvUserId = (TextView) rootView.findViewById(R.id.tvUserId);

		lBalance = (ListView) rootView.findViewById(R.id.lBalance);
		lBalance.setAdapter(adapter);
		lBalance.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

			}
		});

		// bottom bar tab
		imgShedule = (ImageView) rootView.findViewById(R.id.imgShedule);
		imgShedule.setOnClickListener(this);

		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);

		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);

		// highlight the My Account tab
		imgMyAccount.setImageDrawable(getResources().getDrawable(
				R.drawable.my_account_pres));

		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		// bottom bar
		case R.id.imgShedule:
			GoShedule();
			break;
		case R.id.imgAppointment:
			GoAppointments();
			break;
		case R.id.imgMyAccount:
			//
			break;
		case R.id.imgNotification:
			GoNotification();
			break;

		default:
			break;
		}
	}

	/**
	 * Go doctor shedule fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoShedule() {
		DoctorShedule shedule = new DoctorShedule();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, shedule)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoAppointments() {
		DoctorAppointmentList appointment = new DoctorAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, appointment)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go doctor notification fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotification() {
		DoctorNotification notify = new DoctorNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, notify)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
