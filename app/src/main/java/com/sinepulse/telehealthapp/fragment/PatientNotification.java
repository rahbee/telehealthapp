package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient's all notification.
 * patient can see recent different notifications .
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for show notification
 */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.AppointmentBasic;
import com.sinepulse.telehealthapp.adapter.PatientNotificationAdapter;

public class PatientNotification extends Fragment implements OnClickListener {

	ListView lvNotify;

	// bottom bar
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification;

	PatientNotificationAdapter adapter;

	Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.patient_notification,
				container, false);
		mContext = getActivity();
		adapter = new PatientNotificationAdapter(mContext);
		initUI(rootView);
		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {

		lvNotify = (ListView) rootView.findViewById(R.id.lvNotify);
		lvNotify.setAdapter(adapter);

		lvNotify.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				AppointmentBasic();
			}
		});

		// bottom bar
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);

		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);
		imgNotification.setImageDrawable(getResources().getDrawable(
				R.drawable.notifications_pres));
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("Notifications");
	}

	/**
	 * Go appointment detail fragment
	 * 
	 * @param
	 * @return
	 */
	public void AppointmentBasic() {
		Intent basic = new Intent(mContext, AppointmentBasic.class);
		basic.putExtra("RequestType", "patient");
		basic.putExtra("AppointmentStatus", "");
		startActivity(basic);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		// bottom bar
		case R.id.imgFindDoctor:
			GoSearchDoctor();
			break;
		case R.id.imgAppointment:
			patientAppointment();
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;
		case R.id.imgNotification:
			//
			break;

		default:
			break;
		}
	}

	/**
	 * Go patient health fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoHealth() {
		PatientHealth health = new PatientHealth();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, health)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoSearchDoctor() {
		DoctorSearch doctor = new DoctorSearch();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient appointment list fragment
	 * 
	 * @param
	 * @return
	 */
	public void patientAppointment() {
		PatientAppointmentList health = new PatientAppointmentList();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, health)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient account
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		PatientAccount account = new PatientAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

}
