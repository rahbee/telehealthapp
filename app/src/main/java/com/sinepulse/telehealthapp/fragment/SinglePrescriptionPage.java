package com.sinepulse.telehealthapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.PrescriptionInfo;
import com.sinepulse.telehealthapp.adapter.PrescriptionInfoAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvee on 6/25/2016.
 */
public class SinglePrescriptionPage extends Fragment {
    Context mContext;
    View rootView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        rootView = inflater.inflate(R.layout.medicine_list, container,
                false);
        mContext = getActivity();

        List<PrescriptionInfo> data = new ArrayList<PrescriptionInfo>();

        PrescriptionInfo item1 = new PrescriptionInfo();
        item1.name = "Test";
        item1.email = "abc@email.com";
        item1.surname = "Hello";

        data.add(item1);

        PrescriptionInfo item2 = new PrescriptionInfo();
        item2.name = "Test";
        item2.email = "abc@email.com";
        item2.surname = "Hello";

        data.add(item2);

        PrescriptionInfoAdapter prescriptionInfoAdapter = new PrescriptionInfoAdapter(data);
        RecyclerView recList = (RecyclerView) rootView.findViewById(R.id.prescriptionItemList);
        recList.setAdapter(prescriptionInfoAdapter);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(mContext);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        return rootView;
    }
}
