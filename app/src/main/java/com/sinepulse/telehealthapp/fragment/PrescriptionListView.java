package com.sinepulse.telehealthapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.adapter.PrescriptionAdapter;

/**
 * Created by rahbeealvee on 20/06/2016.
 */
public class PrescriptionListView extends Fragment{
    ListView list;
    PrescriptionAdapter prescriptionAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.prescription_list,
                container, false);
        prescriptionAdapter = new PrescriptionAdapter(getActivity());
        list = (ListView) rootView.findViewById(R.id.listPrescription);
        list.setAdapter(prescriptionAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame,new SinglePrescriptionPage()).commit();
                // list.getItemAtPosition(position);
            }
        });

        return rootView;
    }
}
