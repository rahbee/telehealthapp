package com.sinepulse.telehealthapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinepulse.telehealthapp.R;

/**
 * Created by alvee on 6/26/2016.
 */
public class FeedbackForm  extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.feedback_form, container, false);
        return rootView;
    }
}
