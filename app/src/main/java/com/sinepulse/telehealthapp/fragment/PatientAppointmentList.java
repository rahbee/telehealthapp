package com.sinepulse.telehealthapp.fragment;

/**
 * This file is used to show patient's all appointment list.
 * patient can see different types of appointments (pending,completed etc..).
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for show appointments
 * 		 2. take action based on appointment status like 
 * 			i)  if appointment accepted by doctor now it is time to complete payment
 * 			ii) appointment resheduled by doctor now patient can accept or reshedule this appointment 
 * 		3. change appointment data list on change MenuItem 
 */

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.AppointmentBasic;
import com.sinepulse.telehealthapp.adapter.PatientAppointmentAdapter;
import com.sinepulse.telehealthapp.customize.Font;

public class PatientAppointmentList extends Fragment implements OnClickListener {

	Context mContext;
	ListView lvAppointments;
	// bottom bar
	ImageView imgFindDoctor, imgAppointment, imgMyAccount, imgNotification;
	// header
	ImageView imgLeft, imgRight;
	TextView tvStatus;

	PatientAppointmentAdapter adapter;

	public static String appointment_type[] = { "Completed", "Approved",
			"Pending", "Reshedule", "Rejected" };

	public static int appointment_position = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.patient_appointment_list,
				container, false);
		mContext = getActivity();
		adapter = new PatientAppointmentAdapter(mContext);
		initUI(rootView);

		return rootView;
	}

	/**
	 * initialize all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI(View rootView) {
		lvAppointments = (ListView) rootView.findViewById(R.id.lvAppointments);
		lvAppointments.setAdapter(adapter);

		lvAppointments.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				selectedAppointment(position);
			}
		});

		imgLeft = (ImageView) rootView.findViewById(R.id.imgLeft);
		imgLeft.setOnClickListener(this);
		imgRight = (ImageView) rootView.findViewById(R.id.imgRight);
		imgRight.setOnClickListener(this);

		tvStatus = (TextView) rootView.findViewById(R.id.tvDate);
		tvStatus.setText("Completed");
		Font.RobotoRegular(mContext, tvStatus);

		// bottom bar
		imgFindDoctor = (ImageView) rootView.findViewById(R.id.imgFindDoctor);
		imgFindDoctor.setOnClickListener(this);
		imgAppointment = (ImageView) rootView.findViewById(R.id.imgAppointment);
		imgAppointment.setOnClickListener(this);
		imgAppointment.setImageDrawable(getResources().getDrawable(
				R.drawable.appointment_pres));
		imgMyAccount = (ImageView) rootView.findViewById(R.id.imgMyAccount);
		imgMyAccount.setOnClickListener(this);

		imgNotification = (ImageView) rootView
				.findViewById(R.id.imgNotification);
		imgNotification.setOnClickListener(this);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getActivity().setTitle("Appointments");
	}

	/**
	 * Take appropriate action based on appointment status like 
	 * 
	 * i) if appointment accepted by doctor now it is time to complete payment 
	 * ii) appointment resheduled by doctor now patient can accept or reshedule this appointment
	 * 
	 * @param
	 * @return
	 */
	public void selectedAppointment(int position) {
		if (position == 0) { // approved appointment
			Intent go = new Intent(mContext, AppointmentBasic.class);
			go.putExtra("RequestType", "patient");
			go.putExtra("AppointmentStatus", "approved");
			startActivity(go);
		} else if (position == 1) { // reject appointment
			Intent go = new Intent(mContext, AppointmentBasic.class);
			go.putExtra("RequestType", "patient");
			go.putExtra("AppointmentStatus", "rejected");
			startActivity(go);
		} else if (position == 2) { // reshedule appointment
			Intent go = new Intent(mContext, AppointmentBasic.class);
			go.putExtra("RequestType", "patient");
			go.putExtra("AppointmentStatus", "resheduled");
			startActivity(go);
		} else { // 
			Intent go = new Intent(mContext, AppointmentBasic.class);
			go.putExtra("RequestType", "patient");
			go.putExtra("AppointmentStatus", "");
			startActivity(go);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		// inflater.inflate(R.menu.doctor_appointment, menu);
		// super.onCreateOptionsMenu(menu, inflater);

		// (int groupId, int itemId, int order, CharSequence title)
		// for (int i = 0; i < appointment_type.length; i++) {
		// menu.add(0, i, 0, appointment_type[i]);
		// }

		// menu.add(0, 1, 0, "Settings");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		// CommonTask.toast(mContext, 1, "" + item.getItemId());

		// handle item selection
		switch (item.getItemId()) {

		case R.id.mApproved:
			changePosition(0, true);
			// CommonTask.toast(mContext, 1, "toast");
			// tvStatus.setText("Approved");
			break;
		case R.id.mPending:
			changePosition(1, true);
			// tvStatus.setText("Pending");
			break;
		case R.id.mResheduled:
			changePosition(2, true);
			// tvStatus.setText("Resheduled");
			break;
		case R.id.mRejected:
			changePosition(3, true);
			// tvStatus.setText("Rejected");
			break;
		case R.id.mCanceled:
			changePosition(4, true);
			// tvStatus.setText("Cancelled");
			break;
		case R.id.mOverdue:
			changePosition(5, true);
			// tvStatus.setText("Overdue");
			return true;
		case R.id.mCompleted:
			changePosition(6, true);
			// tvStatus.setText("Completed");
			break;

		default:
			// appointment_position = item.getItemId();
			// Log.d(" onOptionsItemSelected ", "" + appointment_position);
			// changePosition(0);
			// return super.onOptionsItemSelected(item);
			break;
		}

		return false;// super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.imgLeft:
			changePosition(1, false);
			break;
		case R.id.imgRight:
			changePosition(2, false);
			break;
		// bottom bar
		case R.id.imgFindDoctor:
			SearchDoctor();
			break;
		case R.id.imgAppointment:
			//
			break;
		case R.id.imgMyAccount:
			GoMyAccount();
			break;

		case R.id.imgNotification:
			GoNotify();
			break;

		default:
			break;
		}
	}

	/**
	 * change next type of appointment on ui[pending,approved] and update
	 * appointment data list
	 * 
	 * @param
	 * @return
	 */
	public void changePosition(int change, boolean isMenuItem) {

		if (isMenuItem) {
			appointment_position = change;
		} else {
			switch (change) {
			case 1:// decrease
				appointment_position--;
				break;
			case 2:// increase
				appointment_position++;
				break;
			default:
				break;
			}
		}

		if (appointment_position >= appointment_type.length) {
			appointment_position = 0;
		} else if (appointment_position < 0) {
			appointment_position = appointment_type.length - 1;
		}

		NextAppointment();

	}

	/**
	 * Go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void NextAppointment() {
		tvStatus.setText(appointment_type[appointment_position]);

		lvAppointments.setAdapter(adapter);
	}

	/**
	 * Go doctor search fragment
	 * 
	 * @param
	 * @return
	 */
	public void SearchDoctor() {
		DoctorSearch doctor = new DoctorSearch();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient notification list fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoNotify() {
		PatientNotification doctor = new PatientNotification();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, doctor)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient health fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoHealth() {
		PatientHealth health = new PatientHealth();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, health)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}

	/**
	 * Go patient account fragment
	 * 
	 * @param
	 * @return
	 */
	public void GoMyAccount() {
		PatientAccount account = new PatientAccount();
		this.getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, account)// ,TAG_FRAGMENT)
				.addToBackStack(null).commit();
	}
}
