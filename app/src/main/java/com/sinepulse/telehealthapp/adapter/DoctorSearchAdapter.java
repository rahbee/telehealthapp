package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show all available doctor's list .
 * patient can see doctor's profile information and available time slot for request an appointment. 
 * use custom font (Roboto Regular, Roboto Medium, Roboto Thin) from Font class for all text
 * @author misba
 * 
 * TODO 1. the information of all doctor should be loaded from server 
 * and accesible from other class. Now static data is used to show time slot.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.activity.DoctorBooking;
import com.sinepulse.telehealthapp.activity.DoctorDetailView;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.thirdparty.CircularImageView;

import java.util.Random;

public class DoctorSearchAdapter extends BaseAdapter {

	private String[] doctors = {"Dr James", "Dr Neil", "Dr Sam", "Dr Max", "Dr John"};
	private String[] orgs = {"Queen Elizabeth II Family Centre", "Auburn Hospital", "Belmont Hospital", "St Vincent's Hospital", "Macquarie Hospital"};
	private String[] degree = {"Specialist paediatric endocrinologist ", "Specialist cardiologist ", "Specialist psychiatrist ", "Specialist neurologist", "Specialist gastroenterologist"};

	Context mContext;

	public DoctorSearchAdapter(Context context) {
		mContext = context;
		// gridAdapter = new GridAdapter(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5; // size of doctor list
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.doctor_search_list_item,
					null);
			holder = new Holder();

			holder.tvDoctorName = (TextView) convertView
					.findViewById(R.id.tvDoctorName);

			holder.tvPosition = (TextView) convertView
					.findViewById(R.id.tvPosition);

			holder.imgDoctor = (CircularImageView) convertView
					.findViewById(R.id.imgDoctor);// tvPulse

			holder.tvSpeciality = (TextView) convertView
					.findViewById(R.id.tvSpeciality);

			holder.tvFee = (TextView) convertView.findViewById(R.id.tvFee);
			Font.RobotoRegular(mContext, holder.tvFee);

			holder.tvDetail = (TextView) convertView
					.findViewById(R.id.tvDetail);
			Font.RobotoThin(mContext, holder.tvDetail);

			holder.tvAvailable = (TextView) convertView
					.findViewById(R.id.tvAvailable);
			Font.RobotoThin(mContext, holder.tvAvailable);

			holder.l_Fee = (LinearLayout) convertView.findViewById(R.id.l_Fee);

			holder.l_Detail = (LinearLayout) convertView
					.findViewById(R.id.l_Detail);
			holder.l_Detail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					DocotrDetail();
				}
			});
			holder.l_Available = (LinearLayout) convertView
					.findViewById(R.id.l_Available);
			holder.l_Available.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					GoAvailableAppointment();
				}
			});
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();


		}

		Font.RobotoMedium(mContext, holder.tvDoctorName);
		Font.RobotoRegular(mContext, holder.tvPosition);
		Font.RobotoRegular(mContext, holder.tvSpeciality);

		if(position<5) {
			holder.tvDoctorName.setText(doctors[position]);
			holder.tvPosition.setText(orgs[position]);
			holder.tvSpeciality.setText(degree[position]);
		}

		holder.tvFee = (TextView) convertView.findViewById(R.id.tvFee);
		// Font.RobotoThin(mContext, holder.tvFee);
		holder.tvFee.setText(Html.fromHtml("<font color=\"#595959\">"
				+ "Fee : " + "</font>" + "<font color=\"#27a2bc\">" + "800 Tk "
				+ "</font>"));

		// holder.imgDoctor.setVisibility(View.VISIBLE);
		// holder.l_Fee.setVisibility(View.VISIBLE);

		return convertView;
	}

	/* private view holder class */
	private class Holder {
		TextView tvDoctorName, tvPosition, tvSpeciality, tvDetail, tvAvailable;
		CircularImageView imgDoctor;
		TextView tvFee;
		LinearLayout l_Fee, l_Detail, l_Available;
	}

	/**
	 * Go Doctor booking fragment to request a appointment on a available time
	 * 
	 * @param
	 * @return
	 */
	public void GoAvailableAppointment() {
		Intent booking = new Intent(mContext, DoctorBooking.class);
		mContext.startActivity(booking);
	}

	/**
	 * Go Doctor detail fragment to see selected doctor's profile information
	 * 
	 * @param
	 * @return
	 */
	public void DocotrDetail() {
		Intent detail = new Intent(mContext, DoctorDetailView.class);
		mContext.startActivity(detail);
	}

}
