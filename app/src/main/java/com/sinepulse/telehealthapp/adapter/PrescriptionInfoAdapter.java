package com.sinepulse.telehealthapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;

import java.util.List;

/**
 * Created by alvee on 6/25/2016.
 */
public class PrescriptionInfoAdapter extends RecyclerView.Adapter<PrescriptionInfoAdapter.PrescriptionViewHolder>{

    private List<PrescriptionInfo> prescriptionInfoList;

    public PrescriptionInfoAdapter(List<PrescriptionInfo> prescriptionInfoList){
        this.prescriptionInfoList = prescriptionInfoList;
    }

    @Override
    public PrescriptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.prescription_card_view, parent, false);

        return new PrescriptionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PrescriptionViewHolder holder, int position) {
        PrescriptionInfo ci = prescriptionInfoList.get(position);
        holder.vName.setText(ci.name);
        holder.vSurname.setText(ci.surname);
        holder.vEmail.setText(ci.email);
        holder.vTitle.setText(ci.name + " " + ci.surname);
    }

    @Override
    public int getItemCount() {
        return prescriptionInfoList.size();
    }

    public static class PrescriptionViewHolder extends RecyclerView.ViewHolder{
        protected TextView vName;
        protected TextView vSurname;
        protected TextView vEmail;
        protected TextView vTitle;

        public PrescriptionViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(R.id.txtName);
            vSurname = (TextView)  v.findViewById(R.id.txtSurname);
            vEmail = (TextView)  v.findViewById(R.id.txtEmail);
            vTitle = (TextView) v.findViewById(R.id.title);
        }
    }
}
