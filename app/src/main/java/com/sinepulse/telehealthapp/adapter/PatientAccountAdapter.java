package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show ptient invoice related information
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for patient invoice related information
 * 		 2. set patient invoice informations on list item view
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class PatientAccountAdapter extends BaseAdapter {

	Context mContext;

	public PatientAccountAdapter(Context context) {
		this.mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.account_list_item, null);
			holder = new Holder();

			holder.tvAmount = (TextView) convertView
					.findViewById(R.id.tvAmount);
			Font.RobotoMedium(mContext, holder.tvAmount);
			holder.tvStatus = (TextView) convertView
					.findViewById(R.id.tvStatus);
			Font.RobotoThin(mContext, holder.tvStatus);
			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			Font.RobotoThin(mContext, holder.tvDate);

			holder.tvAmount.setText(Html.fromHtml("<font color=\"##2ba2bb\">"
					+ "Doctors Fee "
					+ "</font>" + " " + "<font color=\"#8e8e8d\">"
					+ "800Tk" 
					+ "</font>"));
//			+ "<b>"+ "800Tk" + "</b>"  #7f7f7f
			
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		return convertView;
	}


	/* private view holder class */
	private class Holder {
		TextView tvAmount, tvStatus, tvDate;

	}

}
