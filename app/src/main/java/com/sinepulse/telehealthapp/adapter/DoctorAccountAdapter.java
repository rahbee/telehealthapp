package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show doctor account related information.
 * doctor can see his current balance and all invoices of completed transaction.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed for doctor invoice related information
 * 		 2. set doctor invoice informations on ui
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class DoctorAccountAdapter extends BaseAdapter {

	Context mContext;

	public DoctorAccountAdapter(Context context) {
		this.mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 5;// size of trasaction list
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.account_list_item, null);
			holder = new Holder();

			holder.tvAmount = (TextView) convertView
					.findViewById(R.id.tvAmount);
			Font.RobotoMedium(mContext, holder.tvAmount);
			holder.tvStatus = (TextView) convertView
					.findViewById(R.id.tvStatus);
			Font.RobotoThin(mContext, holder.tvStatus);
			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			Font.RobotoThin(mContext, holder.tvDate);

			holder.tvAmount.setText("800Tk");
			holder.tvStatus.setText("Appointment with Dr. Rahbee Alvee");
			holder.tvDate.setText("4.30 PM,04-05-2015");

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		return convertView;
	}

	/* private view holder class */
	private class Holder {
		TextView tvAmount, tvStatus, tvDate;

	}

}
