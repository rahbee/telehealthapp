package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show patient's/doctor's notification list item.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. notification data list from server should be used. Now statis data is used.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class PatientNotificationAdapter extends BaseAdapter {

	private String[] issues = {"Influenza and Pneumonia", "Falls", "Substance Abuse", "Obesity", "Depression", "Overweight and Obesity", "Back Pain"};
	Context mContext;

	public PatientNotificationAdapter(Context context) {
		mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 7;// size of notification list
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.notification_list_item,
					null);// raw_view_bp
			holder = new Holder();

			holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
			Font.RobotoMedium(mContext, holder.tvName);

			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			Font.RobotoThin(mContext, holder.tvDate);
			holder.tvSubject = (TextView) convertView
					.findViewById(R.id.tvSubject);
			Font.RobotoThin(mContext, holder.tvSubject);

			holder.imgPic = (ImageView) convertView.findViewById(R.id.imgPic);// tvPulse
			holder.imgNotify = (ImageView) convertView
					.findViewById(R.id.imgNotify);// tvDeviceName

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.tvName.setText(position + ". Notification for Mr Patient");
		holder.tvSubject.setText(issues[position]);

		return convertView;
	}

	/* private view holder class */
	private class Holder {
		TextView tvName, tvDate, tvSubject;
		ImageView imgPic, imgNotify;

	}
}
