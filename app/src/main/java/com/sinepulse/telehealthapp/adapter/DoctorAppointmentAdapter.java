package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show doctor's appointment list item.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. change background colour depending on appointment status
 * 		 2. use server data to show appointments currenly static data is used.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.telehealthapp.fragment.DoctorAppointmentList;

public class DoctorAppointmentAdapter extends BaseAdapter {

	Context mContext;

	public DoctorAppointmentAdapter(Context context) {
		this.mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 9;// size of appointment list
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.appointment_list_item,
					null);
			holder = new Holder();

			holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
			Font.RobotoMedium(mContext, holder.tvName);
			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			Font.RobotoThin(mContext, holder.tvDate);
			holder.tvSubject = (TextView) convertView
					.findViewById(R.id.tvSubject);
			Font.RobotoThin(mContext, holder.tvSubject);
			// holder.imgIndicator = (ImageView) convertView
			// .findViewById(R.id.imgIndicator);
			holder.imgIndicator = convertView.findViewById(R.id.imgIndicator);

			if (getCurrentState().equalsIgnoreCase("Approved")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.approved_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Pending")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.pending_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Reshedule")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.reshedule_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Rejected")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.reject_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Cancelled")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.cancelled_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Overdue")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.overdue_appointment);
			} else if (getCurrentState().equalsIgnoreCase("Completed")) {
				holder.imgIndicator
						.setBackgroundResource(R.color.complete_appointment);
			}

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		return convertView;
	}

	public String getCurrentState() {
		return DoctorAppointmentList.appointment_type[DoctorAppointmentList.appointment_position];
	}

	/* private view holder class */
	private class Holder {
		TextView tvName, tvDate, tvSubject;
		// ImageView imgIndicator;
		View imgIndicator;

	}

}
