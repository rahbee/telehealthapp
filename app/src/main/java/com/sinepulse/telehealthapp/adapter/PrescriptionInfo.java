package com.sinepulse.telehealthapp.adapter;

/**
 * Created by alvee on 6/25/2016.
 */
public class PrescriptionInfo {
    public String name;
    public String surname;
    public String email;
    static final String NAME_PREFIX = "Name_";
    static final String SURNAME_PREFIX = "Surname_";
    static final String EMAIL_PREFIX = "email_";
}
