package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show all available time slot of particular doctor.
 * from this time slot patient can request for an appointment to this doctor. 
 *
 * @author misba
 * 
 * TODO 1. the data of doctor time slot is different for different doctor's. so time slot should be loaded dynamically 
 * and accesible from other class. Now static data is used to show time slot.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;

public class BookingAdapter extends BaseAdapter {
	private Context mContext;

	String timeSlot[] = { "10.30 AM", "11.00 AM", "11.30 AM", "12.00 AM",
			"12.30 AM", "01.00 PM", "01.30 PM", "02.00 PM", "02.30 PM",
			"03.00 PM", "03.30 PM", "04.00 PM" };

	// Constructor
	public BookingAdapter(Context c) {
		mContext = c;
	}

	@Override
	public int getCount() {
		return timeSlot.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Holder holder = new Holder();
		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.booking_list_item, null);
			holder = new Holder();

			holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.tvTime.setText(timeSlot[position]);
		Font.RobotoRegular(mContext, holder.tvTime);

		return convertView;
	}

	private class Holder {
		TextView tvTime;
	}

}
