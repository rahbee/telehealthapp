package com.sinepulse.telehealthapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by rahbeealvee on 20/06/2016.
 */
public class PrescriptionAdapter extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;

    private String[] names = {"James", "Neil", "Sam"};
    private String[] times = {"4:54pm", "3:55pm", "7:20pm", "6:05pm"};

    public PrescriptionAdapter(Activity a){
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi=view;
        if(view==null)
            vi = inflater.inflate(R.layout.list_prescription_row, null);

        TextView title = (TextView)vi.findViewById(R.id.title); // title
        TextView artist = (TextView)vi.findViewById(R.id.artist); // artist name
        TextView duration = (TextView)vi.findViewById(R.id.duration); // duration
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); // thumb image

        // HashMap<String, String> song = new HashMap<String, String>();
        // song = data.get(i);

        // Setting all values in listview
        title.setText("Prescription " + ++i);
        artist.setText("Dr " + names[new Random().nextInt(3)]);
        duration.setText(times[new Random().nextInt(4)]);
        // imageLoader.DisplayImage(song.get(CustomizedListView.KEY_THUMB_URL), thumb_image);
        return vi;
    }
}
