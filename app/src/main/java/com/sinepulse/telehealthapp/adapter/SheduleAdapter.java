package com.sinepulse.telehealthapp.adapter;

/**
 * This file is used to show shedule of doctor for a particullar date.
 *
 * @author misba
 * 
 * TODO 1. the doctor shedule data is different for different doctor's. so shedule data should be loaded dynamically 
 * and accesible from other class. Now static data is used to show one doctor shedule.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;

public class SheduleAdapter extends BaseAdapter {

	Context mContext;

	private int[] colors = new int[] { 0x30FF0000, 0x300000FF };

	// 1. available 2. booked 3.busy
	int status[] = { 1, 2, 1, 2, 3, 1, 2, 2, 1, 2 };

	public SheduleAdapter(Context context) {
		mContext = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			// convertView = View.inflate(mContext.getApplicationContext(),
			// R.layout.shedule_list_item, null);
			// new Holder();
			convertView = mInflater.inflate(R.layout.shedule_list_item, null);// raw_view_bp
			holder = new Holder();

			holder.tvPatientName = (TextView) convertView
					.findViewById(R.id.tvPatientName);
			holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);

			holder.divider = convertView.findViewById(R.id.divider);
			holder.btnBar = (Button) convertView.findViewById(R.id.btnBar);
			holder.imgNotify = (ImageView) convertView
					.findViewById(R.id.imgNotify);
			holder.imgRight = (ImageView) convertView
					.findViewById(R.id.imgRight);
			holder.btnIcon = (Button) convertView.findViewById(R.id.btnIcon);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.btnIcon.setVisibility(View.INVISIBLE);

		/* show different types of shedule */
		if (status[position] == 1) {// available slot
			holder.divider.setVisibility(View.INVISIBLE);
			holder.imgNotify.setVisibility(View.INVISIBLE);
			holder.tvPatientName.setVisibility(View.INVISIBLE);
			holder.imgRight.setVisibility(View.INVISIBLE);
			holder.btnBar.setVisibility(View.VISIBLE);
		} else if (status[position] == 2) {// booked slot
			holder.tvPatientName.setTextColor(mContext.getResources().getColor(
					R.color.shedule_text_color));
			holder.tvPatientName.setVisibility(View.VISIBLE);
			holder.imgNotify.setImageDrawable(mContext.getResources()
					.getDrawable(R.drawable.notify_icon));
			holder.imgNotify.setVisibility(View.VISIBLE);
			holder.btnBar.setVisibility(View.INVISIBLE);
		} else {// doctor's busy slot
			holder.imgNotify.setImageDrawable(mContext.getResources()
					.getDrawable(R.drawable.busy_red));
			holder.tvPatientName.setText("Busy");
			holder.tvPatientName.setTextColor(mContext.getResources().getColor(
					R.color.busy_text_color));
			holder.imgRight.setVisibility(View.INVISIBLE);
			holder.btnBar.setVisibility(View.INVISIBLE);
		}

		/* alternative row color */
		int colorPos = position % colors.length;

		if (colorPos == 0) {
			convertView.setBackgroundColor(mContext.getResources().getColor(
					R.color.raw_color));
		} else {
			convertView.setBackgroundColor(mContext.getResources().getColor(
					R.color.raw_alter));
		}

		return convertView;
	}

	/* private view holder class */
	private class Holder {
		TextView tvPatientName, tvTime;
		ImageView imgNotify, imgRight;
		Button btnBar, btnIcon;
		View divider;

	}
}
