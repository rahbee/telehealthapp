package com.sinepulse.telehealthapp.slidingmenu;

import java.util.ArrayList;

import com.sinepulse.telehealthapp.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavDrawerAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;

	public NavDrawerAdapter(Context context,
			ArrayList<NavDrawerItem> navDrawerItems) {
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			if (convertView == null) {
				LayoutInflater mInflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater
						.inflate(R.layout.drawer_list_item, null);
			}

			ImageView imgIcon = (ImageView) convertView
					.findViewById(R.id.imgDrawer);
			TextView txtTitle = (TextView) convertView
					.findViewById(R.id.tv_name);

			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
			txtTitle.setText(navDrawerItems.get(position).getTitle());

		} catch (Exception e) {
			// TODO: handle exception
			Log.e("NavDrawerListAdapter", "Error in creating drawer_list_item");
		}

		return convertView;
	}

}