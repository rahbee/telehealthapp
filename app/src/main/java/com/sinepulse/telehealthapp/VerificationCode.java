package com.sinepulse.telehealthapp;

/**
 * This file is used code verification for SignUp .
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 * @TODO 1. api implementation needed 
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sinepulse.telehealthapp.R;
import com.sinepulse.telehealthapp.customize.Font;
import com.sinepulse.telehealthapp.customize.Validation;

public class VerificationCode extends Activity implements OnClickListener {

	EditText etVerificationCode;
	Button btnSubmit;
	TextView tvTermCondition, tvResend;
	TextView code_txt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verification_code);

		initUI();
	}

	/**
	 * initial all ui component with xml
	 * 
	 * @param
	 * @return
	 */
	public void initUI() {
		etVerificationCode = (EditText) findViewById(R.id.etVerificationCode);
		Font.RobotoRegular(VerificationCode.this, etVerificationCode);
		btnSubmit = (Button) findViewById(R.id.btnSubmit);
		Font.RobotoRegular(VerificationCode.this, btnSubmit);
		btnSubmit.setOnClickListener(this);

		// agreement message
		tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);
		Font.RobotoThin(VerificationCode.this, tvTermCondition);
		tvTermCondition.setText(Html.fromHtml("<font color=\"#000000\">"
				+ "I agree to the " + "</font>" + "<font color=\"#27a2bc\">"
				+ "terms " + "</font>" + "<font color=\"#000000\">" + "and "
				+ "</font>" + "<font color=\"#27a2bc\">" + "conditions "
				+ "</font>"));

		// Resends message
		tvResend = (TextView) findViewById(R.id.tvResend);
		Font.RobotoRegular(VerificationCode.this, tvResend);
		tvResend.setText(Html
				.fromHtml("<font color=\"#bfbfc1\">"
						+ "Check your email to get verification code \n Didn\'t get it? "
						+ "</font>" + "<font color=\"#666666\">"
						+ " Tap to Resend" + "</font>"));

		code_txt = (TextView) findViewById(R.id.code_txt);
		Font.RobotoRegular(VerificationCode.this, code_txt);
	}

	/**
	 * user confirmation message after successful code verification.
	 *
	 * @param
	 * @return
	 * 
	 *         TODO api implementation needed for complete Registration
	 */
	@SuppressLint("InflateParams")
	public void accountActivationAlert(final boolean status) {
		// Create custom dialog object
		final Dialog dialog = new Dialog(this);
		// hide to default title for Dialog
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// inflate the layout dialog_layout.xml and set it as contentView
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.account_activation_view, null,
				false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);

		// dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		// Retrieve views from the inflated dialog layout and update their
		// values
		TextView tvAlertTitle = (TextView) dialog
				.findViewById(R.id.tvAlertTitle);
		Font.RobotoMedium(VerificationCode.this, tvAlertTitle);

		if (status) {
			tvAlertTitle.setText(getResources().getString(
					R.string.title_msg_success));
		} else {
			tvAlertTitle.setText(getResources().getString(
					R.string.title_msg_failed));
		}

		TextView tvAlertMsg = (TextView) dialog.findViewById(R.id.tvAlertMsg);
		Font.RobotoRegular(VerificationCode.this, tvAlertMsg);

		if (status) {
			tvAlertMsg.setText(getResources().getString(R.string.success_msg));
		} else {
			tvAlertMsg.setText(getResources().getString(R.string.failed_msg));
		}

		Button btnAlert = (Button) dialog.findViewById(R.id.btnAlert);
		Font.RobotoRegular(VerificationCode.this, btnAlert);

		if (status) {
			btnAlert.setText(getResources().getString(R.string.success_btn_msg));
		} else {
			btnAlert.setText(getResources().getString(R.string.failed_btn_msg));
		}

		btnAlert.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (status) {
					GoNext();
				} else {

				}
				// Dismiss the dialog
				dialog.dismiss();
			}
		});

		// Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
		// btnCancel.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// // Close the dialog
		// dialog.dismiss();
		// }
		// });

		// Display the dialog
		dialog.show();
	}

	/**
	 * Go Login page
	 * 
	 * @param
	 * @return
	 */
	public void GoLogin() {
		Intent login = new Intent(VerificationCode.this, Login.class);
		startActivity(login);
	}

	/**
	 * Notify user about registration process successful or not
	 * 
	 * @param
	 * @return
	 * 
	 */
	@SuppressLint("InflateParams")
	public void showCustomDialog() {
		// Create custom dialog object
		final Dialog dialog = new Dialog(this);
		// hide to default title for Dialog
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// inflate the layout dialog_layout.xml and set it as contentView
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.dialog_layout, null, false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setContentView(view);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

		// Retrieve views from the inflated dialog layout and update their
		// values
		TextView txtTitle = (TextView) dialog
				.findViewById(R.id.txt_dialog_title);
		txtTitle.setText("Custom Dialog");

		TextView txtMessage = (TextView) dialog
				.findViewById(R.id.txt_dialog_message);
		txtMessage
				.setText("Do you want to visit the website : https://www.android-ios-tutorials.com ?");

		Button btnOpenBrowser = (Button) dialog
				.findViewById(R.id.btn_open_browser);
		btnOpenBrowser.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// Open the browser
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://www.android-ios-tutorials.com"));
				startActivity(browserIntent);
				// Dismiss the dialog
				dialog.dismiss();
			}
		});

		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close the dialog
				dialog.dismiss();
			}
		});

		// Display the dialog
		dialog.show();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSubmit:
			checkInput();
			break;

		default:
			break;
		}
	}

	/**
	 * Checking for valid user input and complete registration process.
	 *
	 * @param
	 * @return
	 * 
	 *         TODO api implementation needed for complete Registration
	 */
	public void checkInput() {

		if (Validation.isValidate) {
			if (Validation.IsEmpty(etVerificationCode)) {
				etVerificationCode.setError("Verification code needed");
				return;
			}
		}

		if (!Validation.IsEmpty(etVerificationCode)) {
			accountActivationAlert(true);
			return;
		}

		// Here server varification needed

		GoNext();

	}

	/**
	 * Go Login page
	 * 
	 * @param
	 * @return
	 */
	public void GoNext() {
		Intent next = new Intent(VerificationCode.this, Login.class);
		startActivity(next);
	}

}
