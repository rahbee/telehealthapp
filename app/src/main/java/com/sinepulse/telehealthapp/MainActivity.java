package com.sinepulse.telehealthapp;

/**
 * This file is used to login module.
 *
 * Roboto Regular, Roboto Medium, Roboto Thin used for text
 * @author misba
 * 
 */

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinepulse.telehealthapp.fragment.ChangePassword;
import com.sinepulse.telehealthapp.fragment.FeedbackForm;
import com.sinepulse.telehealthapp.fragment.PrescriptionListView;
import com.sinepulse.telehealthapp.fragment.ProfileViewDoctor;
import com.sinepulse.telehealthapp.fragment.DoctorShedule;
import com.sinepulse.telehealthapp.fragment.PatientHealth;
import com.sinepulse.telehealthapp.fragment.ProfileViewPatient;
import com.sinepulse.telehealthapp.fragment.DoctorSearch;
import com.sinepulse.telehealthapp.slidingmenu.NavDrawerAdapter;
import com.sinepulse.telehealthapp.slidingmenu.NavDrawerItem;

@SuppressLint("InflateParams")
public class MainActivity extends Activity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;
	private Boolean IS_DOCTOR;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer_layout);

		mTitle = mDrawerTitle = getTitle();

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		/**
		 * get intent to decide user type doctor or patient and load doctor
		 * navigation drawer item or patient navigation item accordingly
		 */
		IS_DOCTOR = getIntent().getBooleanExtra("user_type", false);

		if (IS_DOCTOR) {

			DoctorDrawerList();
		} else {
			PatienDrawerList();
		}
		// What's hot, We will add a counter here
		// navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
		// navMenuIcons.getResourceId(5, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		/*
		 * set a custom shadow that overlays the main content when the drawer
		 * opens mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
		 * GravityCompat.START); set up the drawer's list view with items and
		 * click listener mDrawerList.setAdapter(new ArrayAdapter<String>(this,
		 * android.R.layout.simple_list_item_1, mPlanetTitles));
		 */

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		adapter = new NavDrawerAdapter(getApplicationContext(), navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// add header on navigation item list
		addHeader();

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		/*
		 * ActionBarDrawerToggle ties together the the proper interactions
		 * between the sliding drawer and the action bar app icon
		 */
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.menu, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {
			@Override
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);

				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			/**
			 * load DoctorSearch fragment for patient and DoctorSchedule
			 * fragment for doctor initially
			 */
			selectItem(9);
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	// drawer header view
	RelativeLayout rBirthDay;
	TextView tv_drawer_birth_day, tv_drawer_blood_type, tv_drawer_name,
			tv_drawer_account_balance;

	/**
	 * add a custom view in navigation items list for both (doctor and patient)
	 * 
	 * @param
	 * @return
	 */
	public void addHeader() {
		View header = getLayoutInflater().inflate(R.layout.drawer_header, null);

		rBirthDay = (RelativeLayout) header.findViewById(R.id.rBirthDay);
		tv_drawer_birth_day = (TextView) header
				.findViewById(R.id.tv_drawer_birth_day);
		tv_drawer_blood_type = (TextView) header
				.findViewById(R.id.tv_drawer_blood_type);
		tv_drawer_name = (TextView) header.findViewById(R.id.tv_drawer_name);

		tv_drawer_account_balance = (TextView) header
				.findViewById(R.id.tv_drawer_account_balance);

		if (IS_DOCTOR) {
			rBirthDay.setVisibility(View.GONE);
		} else {
			rBirthDay.setVisibility(View.VISIBLE);
		}

		mDrawerList.addHeaderView(header);
	}

	/**
	 * load navigation drawer item for doctor
	 * 
	 * @param
	 * @return
	 */
	private void DoctorDrawerList() {
		navMenuTitles = getResources().getStringArray(
				R.array.doctor_drawer_menu);
		navMenuIcons = getResources().obtainTypedArray(
				R.array.doctor_drawer_menu_icon);

		// profile
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1), false));
		// Change Password
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1), false));
		// About
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1), false));

		// Feedback
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1), true));
		// Logout
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1), false));
		// Logout
		//navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
		//		.getResourceId(5, -1), false));

	}

	/**
	 * load navigation items for patient
	 * 
	 * @param
	 * @return
	 */
	private void PatienDrawerList() {
		navMenuTitles = getResources().getStringArray(
				R.array.patient_drawer_menu);// nav_drawer_items_patient
		navMenuIcons = getResources().obtainTypedArray(
				R.array.patient_drawer_menu_icon);// nav_drawer_icons_patient

		// patient profile
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1), false));
		// Change Password
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1), false));
		// My Health
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1), false));
		// Prescriptions, Will add a counter here
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1), false));
		// Feedback
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1), true));
		// Logout
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1), false));
		// Logout
		//navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
		//		.getResourceId(6, -1), false));
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater inflater = getMenuInflater();

		// inflater.inflate(R.menu.main, menu);
		// return super.onCreateOptionsMenu(menu);

		// inflater.inflate(R.menu.main, menu);
		return false;
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return true;
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() > 0) {
			Log.i("MainActivity", "popping backstack");
			fm.popBackStack();
		} else {
			Log.i("MainActivity", "nothing on backstack, calling super");
			super.onBackPressed();
		}
	}

	FragmentManager fragmentManager;

	/**
	 * Go next fragment depend on navigation item selection for doctor and
	 * patient.
	 * 
	 * @param position
	 * @return
	 */
	@SuppressWarnings("unused")
	private void selectItem(int position) {

		// closeContextMenu();
		if (position <= navMenuTitles.length) {
			// setTitle(navMenuTitles[position]);
		}

		// getActionBar().setLogo(navDrawerItems.get(position).getIcon());
		mDrawerLayout.closeDrawer(mDrawerList);

		Fragment fragment = null;
		boolean status = true;
		if (IS_DOCTOR) {/* Doctor Navigation */
			switch (position) {

			case 0:// drawer header

				break;
			case 1:// My Profile
				fragment = new ProfileViewDoctor();
				break;
			case 2:// Change Password

				break;
			case 3:// About

				break;
			case 4:// Feedback
				fragment = new FeedbackForm();
				break;
			case 5:// Help
				logout();
				break;
			case 6:// Logout
					// status = false;
				logout();
				break;
			default:// My Shedule
				fragment = new DoctorShedule();
				break;
			}
		} else {
			switch (position) {/* Patient Navigation */

			case 0:// drawer header

				break;
			case 1:// My Profile
				fragment = new ProfileViewPatient();
				break;
			case 2:// Change Password
				fragment = new ChangePassword();
				break;
			case 3:// My Health
				fragment = new PatientHealth();
				break;
			case 4:// Prescription
				fragment = new PrescriptionListView();
				break;
			case 5:// Feedback
				fragment = new FeedbackForm();
				break;
			case 6:// Help
				logout();
				break;
			case 7:// Logout
				logout();
				break;
			default:// Doctor Search
				fragment = new DoctorSearch();
				// setTitle("Doctor Search");
				break;
			}
		}

		if (fragment != null) {
			fragmentManager = getFragmentManager();

			fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment).addToBackStack(null)
					.commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			if (position <= navMenuTitles.length) {
				// setTitle(navMenuTitles[position]);
				getActionBar().setLogo(navDrawerItems.get(position).getIcon());
			}

			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	/**
	 * user(Doctor/Patient) logout implementation
	 * 
	 * @param
	 * @return
	 */
	public void logout() {

		Intent intent = new Intent(this, Login.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish(); // call this to finish the current activity
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}
